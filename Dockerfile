FROM --platform=linux/amd64 index.alauda.cn/alaudak8s/alauda-theme:latest as builder

COPY . /content

WORKDIR /content

RUN hugo --ignoreCache \
  --ignoreVendor \
  --config artifacts/config.toml \
  --cleanDestinationDir \
  --theme=alauda-theme \
  --themesDir=/ \
  -s /content


FROM alpine:3.11
ARG TARGETPLATFORM

ENV PORT=80 \
    SITE_ROOT=/public
ENTRYPOINT ["/usr/bin/caddy"]
CMD ["-conf", "/Caddyfile", "-root", "${SITE_ROOT}"]
COPY bin/${TARGETPLATFORM:-linux/amd64}/caddy /usr/bin/

COPY artifacts/Caddyfile /
COPY --from=builder /content/public /public/acp-docs
COPY --from=builder /content /original