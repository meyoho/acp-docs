+++
title = "平台管理"
description = ""
weight = 5
alwaysopen = false
+++

平台管理页面仅对拥有平台管理员、平台审计人员角色的用户可见。

在平台管理页面，您可管理平台的用户、角色、IDP、许可证，并可查看平台的运营统计数据和平台的审计数据。


{{%children style="card" description="true" %}}




