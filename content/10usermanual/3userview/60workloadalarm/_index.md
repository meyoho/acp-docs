+++
title = "计算组件告警管理"
description = "当应用及计算（Deployment、DaemonSet、StatefulSet）所在集群部署了监控服务时，可为当前命名空间下的计算组件创建并管理告警。"
weight = 60
+++

当应用及计算（Deployment、DaemonSet、StatefulSet）所在集群部署了监控服务时，可为当前命名空间下的计算组件创建告警并管理告警。

支持在业务视图中，为计算组件创建指标告警、自定义告警、日志告警。

同时，如果在平台的管理视图创建了基于计算组件告警指标的告警模板，可在业务视图中通过模板快速为计算组件创建告警。

{{%children style="card" description="true" %}}





