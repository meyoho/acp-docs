+++
title = "模板应用"
description = "平台上基于应用模板（Chart）部署的应用被称为模板应用。"
weight = 20
alwaysopen = false
+++

平台上基于应用模板（Chart）部署的应用被称为模板应用。

相比平台上的应用，模板应用的特点如下：

* 基于已有的应用模板创建，创建过程更为简单、快捷。

* 关联的资源都会在模板中定义，创建应用时一起创建。



{{%children style="card" description="true" %}}

