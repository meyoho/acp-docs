+++
title = "更新默认证书"
description = "为 HTTPS 协议的监听端口更新默认证书。"
weight = 7
+++


为 HTTPS 协议的监听端口更新默认证书。


**操作步骤**

1. 登录平台，切换至业务视图，进入待操作的项目和命名空间。

2. 单击 **网络** > **负载均衡**，进入负载均衡器列表页面。

      **提示**：在顶部导航栏可通过单击集群下拉选择框切换当前项目。
      
3. 在 **负载均衡** 列表页面，单击待更新默认证书端口所属负载均衡器的名称，进入详情页面。

3. 在侦听器信息栏，单击待更新默认证书的 HTTPS 监听端口名称，进入监听端口详情页面。

4. 单击 **基本信息** 栏右上角的 **操作** 下拉按钮，在展开的下拉列表中选择 **更新默认证书**。

4. 在弹出的 **更新默认证书** 对话框中，单击默认证书右侧的下拉选择框，选择证书。

7. 配置完成后，单击 **确定** 按钮。