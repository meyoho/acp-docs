+++
title = "内部路由"
description = "内部路由即 Kubernetes Service，能够实现计算组件内部发现的功能。"
weight = 1
alwaysopen = false
+++

内部路由即 Kubernetes Service，能够实现计算组件内部发现的功能。

Kubernetes Service 定义了一组 Pod 的逻辑集合，支持设定计算组件在集群内部的访问策略，相当于集群内部的微服务。Kubernetes Service 有一个固定 IP 地址，为其它计算组件或访问者提供位于集群内部的统一访问入口。

通常情况下，Kubernetes Service 使用标签选择器（Label Selector），根据设定的访问策略，将内部流量转发到具有相同标签的（label）各个 Pod，同时，Service 可以给这些 Pod 做负载均衡。

更多内容，请参见 [Kubernetes service](https://kubernetes.io/docs/concepts/services-networking/service/?spm=a2c4g.11186623.2.10.e4f2353fF124b0)。




{{%children style="card" description="true" %}}

