+++
title = "访问规则"
description = "访问规则通过使用 Kubernetes Ingress，将集群外部的 HTTP/HTTPS 路由暴露给集群内部的 Kubernetes Service，实现计算组件外部访问权控制的功能。"
weight = 2
alwaysopen = false
+++

访问规则通过使用 Kubernetes Ingress（访问规则），将集群外部的 HTTP/HTTPS 路由暴露给集群内部的 Kubernetes Service，实现计算组件外部访问权控制的功能。

**注意**：对于 HTTP 协议，Ingress 仅支持将 80 端口作为对外端口。对于 HTTPS 协议，Ingress 仅支持将 443 端口作为对外端口。平台的负载均衡会自动添加 80 和 443 监听端口。

要使用 Ingress，集群中必须部署 Ingress Controller（访问控制器），它负责监听 Ingress 和 Service 的变化。新创建 ingress 后，Ingress Controller 中会自动生成一条和 Ingress 匹配的转发规则。当 Ingress Controller 接收到请求时，它会匹配访问规则中的转发规则，并分发流量至指定的 Service（内部路由），如下图所示。

<img src="/img/netrelationship.png" width = "500" />

如果集群中创建了 ALB，ALB（Alauda LoadBalancer）可以作为集群的访问控制器。

**说明**：如果集群中未部署访问控制器，在业务视图中也支持创建访问规则，但是无法根据访问规则转发流量。



{{%children style="card" description="true" %}}

