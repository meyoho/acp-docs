+++
title = "更新访问规则"
description = "更新已创建的访问规则。"
weight = 3
+++


更新已创建的访问规则。不支持更新名称。

**操作步骤**

1. 登录平台，切换至业务视图，进入访问规则所在的项目。

2. 在左导航栏顶部，选择访问规则所在的命名空间。

3. 单击 **网络** > **访问规则**，在访问规则列表页面，找到要更新的访问规则，支持根据名称进行搜索。单击 **操作** 栏下的 ![](/img/003point.png)，再单击 **更新**。

4. 在更新访问规则页面，支持更新除名称外的其它访问规则参数。参考 [创建访问规则]({{< relref "10usermanual/3userview/30netuserview/2ingress/1ingresscreate.md" >}})。

5. 完成更新内容后，单击 **更新**。
