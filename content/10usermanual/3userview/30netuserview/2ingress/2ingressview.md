+++
title = "查看访问规则详情"
description = "查看已创建访问规则的详细信息。"
weight = 2
+++

查看已创建访问规则的详细信息。

**操作步骤**

1. 登录平台，切换至业务视图，进入访问规则所在的项目。

2. 在左导航栏顶部，选择访问规则所在的命名空间。

3. 单击 **网络** > **访问规则**，进入访问规则列表页面。

4. 在访问规则列表页面，单击待查看的 ***访问规则名称***，进入访问规则的详情页面。

   **或**：单击待查看的访问规则记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **查看**。

   **提示**：在访问规则列表页面，右上角的搜索框中支持输入访问规则名称进行搜索。

5. 在访问规则详情页面，单击 **详情信息** 页签，查看访问规则的信息。

  * 在 **基本信息** 区域，查看访问规则的基本信息。<br>同时，支持： 
  
  		**查看所属应用详情**：当前资源属于某个应用时，单击 **所属应用** 右侧的 ***应用名称*** 将跳转应用的详情页面，可查看应用的详细信息。
  	
  * 在 **规则** 区域，查看访问规则的访问规则列表。
  	
  	* **地址**：地址=域名+访问路径，例如：`test.com/testpath`。

  	* **内部路由**：地址映射的内部路由。

  	* **端口**：内部路由的指定端口。

  * 在 **HTTPS 证书** 区域，查看与访问规则设置的域名相关联的 HTTPS 证书信息。  
  如果域名需要通过 HTTPS 认证访问，应将 HTTPS 证书保存到 Kubernetes Secret（保密字典）中，并在创建访问规则时，与访问规则关联。

6. 单击 **YAML** 页签，在 **YAML（只读）** 区域，查看 Kubernetes Ingress 的 YAML 编排文件，同时支持以下操作：

  * 单击 **导出**，将 YAML 导出保存成一个文件。 
  	
  * 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
  	
  * 单击 **复制**，复制编辑内容。

  * 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 

  * 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。

7. 单击 **事件** 页签，在 **事件** 页面，查看与当前访问规则相关的所有事件信息。
   	

