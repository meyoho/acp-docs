+++
title = "网络"
description = ""
weight = 30
alwaysopen = false
+++

在业务视图中，您可以通过管理内部路由定义一组 Pod 的逻辑集合和访问策略。而通过设置访问规则可以为内部路由提供集群外部访问的 URL、负载均衡、SSL 终止、HTTP/HTTPS 路由。


{{%children style="card" description="true" %}}

