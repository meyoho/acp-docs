+++
title = "网络策略（Alpha）"
description = "。"
weight = 6
alwaysopen = false
+++

网络策略（NetworkPolicy）是依赖网络插件实现的，从属于命名空间（Namespace） Kubernetes 资源。出于安全、限制网络流量的目的，通过 Pod 选择器（podSelector）和网络流量进（ingress）出（egress） Pod 的规则，实现网络隔离并减少攻击面。

当访问 Pod 时，要满足以下条件之一，才能访问成功。

* Pod 没有被任何网络策略选中，未被任何网络策略选中的 Pod 的访问将不受限制。

* 访问来自 Pod 所在的主机。

* 访问条件满足网络策略里 ingress 字段下定义的某条规则。

更多内容，请参考 [Kubernetes NetworkPolicy](https://kubernetes.io/docs/concepts/services-networking/network-policies/)。

{{%children style="card" description="true" %}}



