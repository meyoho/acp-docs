+++
title = "创建网络策略"
description = "创建网络策略（Kubernetes Ingress），通过 Pod 选择器（podSelector）和网络流量进（ingress）出（egress） Pod 的规则，实现网络隔离并减少攻击面。"
weight = 2
+++

创建网络策略（Kubernetes Ingress），通过 Pod 选择器（podSelector）和网络流量进（ingress）出（egress） Pod 的规则，实现网络隔离并减少攻击面。

基于常见的使用场景，平台提供了 12 个网络策略模板，通过选择模板并修改 podSelector、ingress、egress 相关参数即可快速创建网络策略。

**操作步骤**

1. 登录平台，切换至业务视图，并进入要创建网络策略的项目和命名空间。

3. 单击 **网络** > **网络策略**，进入网络策略列表页面。

4. 单击 **创建网络策略** 按钮。

5. 在弹出的选择网络策略模板对话框中，单击选则模板。

	**说明**：基于常见的使用场景，平台提供了 12 个默认的网络策略模板供您选择。

6. 单击 **确定** 按钮，进入 **YAML（读写）** 页面。

	在 **YAML（读写）** 页面，您可以基于模板已有的参数，参照 [Kubernetes 官方说明](https://kubernetes.io/docs/concepts/services-networking/network-policies/) 或 [配置示例说明]({{< relref "10usermanual/3userview/30netuserview/6networkpolicy/2npcreate.md#1" >}}) 配置 `metadata.name`、podSelector、ingress、egress 等参数。
	
	同时，还可以执行以下操作：
	
	* 单击 **导入**，选择已有的 YAML 文件内容作为编辑内容。

	* 单击 **导出**，将 YAML 导出保存成一个文件。 

	* 单击 **清空**，清空所有编辑内容，会同时清空表单中的编辑内容。
	
	* 单击 **恢复**，回退到上一次的编辑状态。	
		
	* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
	* 单击 **复制**，复制编辑内容。

	* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
	
	* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。

4. 单击 **创建** 按钮。  
	

<span id="1">**配置示例**</span>

以下以 **某些 namespace 的某些 pod 可访问指定 pod** 模板参数为例进行说明。

```
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: allow-some-other-namespace
spec:
  podSelector:
    matchLabels:
      role: db
  ingress:
    - from:
        - namespaceSelector:
            matchLabels:
              env: production
          podSelector:
            matchLabels:
              app: mail
```

示例中的网络策略规则为：允许标签中含有 `env: production` 的命名空间下，标签中含有 `app: mail` 的 Pod，访问标签中含有 ` role: db` 的所有 Pod。

**参数说明**

<style>table th:first-of-type { width: 30%;}</style> 

<style>table th:nth-of-type(2) { width: 10%;}</style>

<style>table th:nth-of-type(3) { width: 10%;}</style>

| 名称               | 类型     | 是否必填项 | 描述   |
| ---------------- | ------ | ----- | ---- |
| **apiVersion** | string |  是 |  Kubernetes API 的版本号。|
| **kind** | string |  是 |  资源对象的类型。网络策略的类型为 `NetworkPolicy `。|
| **metadata** | object |  是 |  元数据。|
| metadata.**name** | string |  是 |  网络策略的名称。创建时可修改。|
| **spec** | object | 是 | 详细信息。|
| spec.**policyTypes** | array | 否 |策略类型。<br>Ingress：入口规则，允许流量匹配 `from ` 和 `ports` 部分；<br>Egress：出口规则，允许流量匹配 `to ` 和 `ports` 部分。|
| spec.**ingress/egress** | array | 否 |Ingress 或 Egress 规则的可匹配的部分的信息。|
| ingress.**from** | array | 否 | 流量来源的信息。|
| egress.**to** | array | 否 | 流量外出的信息。|
| from.**namespaceSelector** | object | 否 |集群命名空间标签选择器。通过集群的范围标签选择命名空间，将会匹配通过该标签选择器选择的所有命名空间中的所有 Pods。<br>如果该值为空，表示匹配所有命名空间。 |
| to.**ipBlock** | object | 否 |IPBlock 描述了一个特殊的 CIDR （例如：“192.168.1.1 .1/24”），它可以被 NetworkPolicySpec 的 podSelector 匹配。 |
| ipBlock.**cidr** | string | 否 |CIDR 是一个代表 IP 块的字符串，例如：“192.168.1.1/24”。 |
| namespaceSelector.**matchLabels** | object |否 | 匹配标签键值对信息。格式如：`"键": "值"`。|
| from.**podSelector** | object | 否 | Pod 标签选择器。通过标签选择匹配通过该标签选择器选择的所有 Pods。<br>如果该值为空，表示匹配所有 Pods。 |
| podSelector.**matchLabels** | object | 否 | 匹配标签键值对信息。格式如：`"键": "值"`。|
| ingress.**ports** | array | 否 |   端口的列表信息。|
| ports.**protocol** | string | 否 | 协议。|
| ports.**port** | integer | 否 | 端口号。|
