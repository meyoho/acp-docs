+++
title = "计算"
description = "平台的普通用户可以在业务视图中，创建定时任务（Cronjob），在给定需要的时间点执行非持续性的业务，例如：定时备份、清理等。"
weight = 10
+++

平台业务视图的计算功能模块为平台上的 WorkLoad（工作负载）提供了功能入口，包括：部署（Deployment）、守护进程集（DaemonSet）、有状态副本集（StatefulSet）、定时任务（CronJob）、任务记录（Job）、TApp。您可以通过平台提供的功能入口快速管理平台上的计算资源。

其中，可通过（Deployment）、守护进程集（DaemonSet）、有状态副本集（StatefulSet）功能入口管理平台上应用下的相应计算资源。



{{%children style="card" description="true" %}}





