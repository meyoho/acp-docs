+++
title = "更新 Tapp"
description = "更新已创建的 Tapp 的信息。"
weight = 5

+++

更新已创建的 Tapp 的信息，例如：显示名称、实例数量、更新策略、容器相关信息等，不支持更新 Tapp 名称。

**操作步骤**

1. 登录平台，切换至业务视图，进入待更新 Tapp 所在的命名空间。

1. 单击左侧导航栏中的 **计算** > **Tapp**，进入 Tapp 列表页面。

2. 单击待更新 Tapp 记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **更新**。

	**或**：
	
	1. 单击更新看 ***Tapp 的名称***，进入 Tapp 的详情页面。
	
	2. 单击页面右上角的 **操作** 下拉按钮，并选择 **更新**。

1. 在更新 Tapp 页面，参照 [创建 Tapp]({{< relref "10usermanual/3userview/10calculate/5tapp/2tappcreate.md" >}}) 的参数说明，更新相关参数。



    

