+++
title = "通过 YAML 创建 Tapp"
description = "通过 YAML 创建一个 Tapp。"
weight = 3
+++

如果您熟悉 YAML 编排文件的编写方式，通过导入或编写 YAML 文件，快速创建一个 Tapp。

**操作步骤**

1. 登录平台，切换至业务视图，进入待创建 Tapp 的命名空间。

1. 单击左导航栏中的 **计算** > **Tapp**，打开 Tapp 列表页面。

2. 单击 **创建 Tapp** 按钮右侧的 ![](/img/dropdown.png)，选择 **YAML 创建**。

10. 在 **YAML（读写）** 区域，输入 Tapp 的 YAML 编排文件内容，同时支持以下操作：

    - 单击 **导入**，导入已有的 YAML 文件内容作为编辑内容。

    - 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 

    - 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。

      **提示**：
      
	* 在 **YAML（读写）** 区域下方 YAML 样例右侧的 ![](/img/input.png) 图标，可快速输入样例 YAML。
	
	* 单击 ![](/img/00blueeye.png) 图标，在弹出的 YAML 样例窗口，可预览样例 YAML 的内容。同时，支持以下操作：
	
		* 单击 **导出**，将 YAML 导出保存成一个文件。 
		
		* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
		* 单击 **复制**，复制编辑内容。
		
		* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
		
		* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。 
      

11. 确认信息无误后，单击 **创建** 按钮。