+++
title = "Tapp（Alpha）"
description = "Tapp 是利用 Kubernetes CRD 的功能实现的扩展 API，一种自定义类型的 WorkLoad（工作负载）。"
weight = 8
+++

Tapp 是利用 Kubernetes CRD 的功能实现的扩展 API，一种自定义类型的 WorkLoad（工作负载）。

Tapp 可运行有状态、无状态应用，所属容器组实例具有可以标识的 ID，不同的容器组实例支持使用不同的配置，实现同时运行多版本。

相较 Deployment 和 StatefulSet，Tapp 的功能特点如下：

|  功能特点 |  Deployment  |  StatefulSet   |  Tapp  |
|  -----  |  --------  |  -------  | ------ |
| Pod 唯一性  |  无  |  每个 Pod 有唯一标识  |    每个 Pod 有唯一标识  |
|   Pod 存储独占 | 仅支持单个容器 |  支持 |  支持  |
| 存储随 Pod 迁移  |  不支持  |  支持  |  支持  |
| 自动扩缩容  |  支持  |  不支持  |  支持  |
| 批量升级容器  |   支持  |  不支持  |  支持  |
| Pod 严格按顺序更新  |   不支持  |  支持  |  不支持  |
| 自动迁移问题节点 Pod   |   支持  |  不支持  |  支持  |
| Pod 多版本管理  |   同时只有一个版本  |  可保持两个版本  |  可保持多个版本  |
| Pod 原地灰度升级  |  不支持  |  不支持  |  支持  |
| IP 随 Pod 迁移  |  不支持  |  支持  |  支持  |


您可在平台上创建、更新、删除 Tapp 或预览 Tapp 的信息。


{{%children style="card" description="true" %}}





