+++
title = "有状态副本集"
description = "守护进程集（DaemonSet）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过平台创建一个 DaemonSet 来运行一个程序。也可以在应用中创建多个 DaemonSet 作为应用下的计算组件来为应用提供特定的服务。"
weight = 3

+++

有状态副本集（StatefulSet）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过平台创建一个 StatefulSet 来运行一个程序。也可以在应用中创建多个 StatefulSet 作为应用下的计算组件来为应用提供特定的服务。

StatefulSet 可用于管理有状态应用（Stateful Application）。

通常 Pod 实例具备主从关系、主备关系的应用叫做有状态应用，例如： MySQL、MongoDB 等。这类应用下的部分实例会在本地磁盘中保存一份数据，这类 Pods 一旦被删除，即便被重建也会导致数据丢失或数据和 Pod 之间的对应关系丢失，从而导致应用运行失败。

和 Deployment 相比，Deployment 下的所有 Pods 都是一样的，Pods 之间没有顺序，会被随机的调度到符合条件的主机上。需要时，创建新的 Pod；不需要时，随意删除任意的 Pod，可能导致重要数据丢失。而 Kubernetes 在 Deployment 的基础上扩展出的 StatefulSet，在 Pod 被重新创建时，能够恢复 Pod 的状态，满足有状态应用的实际使用场景。   

**StatefulSet 的功能特点如下**：

* StatefulSet 所管理的 Pods 拥有固定的网络标识（hostname）和 [0，N) 范围内的唯一的数字序号。Pod 有一定的启动、停止、删除、滚动更新顺序和网络一致性，即便是重新创建的 Pod 也会按照原来的顺序启动、停止、删除、滚动更新，同时，网络标识和重新创建前一样，能够为新创建的 Pod 恢复原有状态，确保新 Pod 能够被正常访问到。

* StatefulSet 所管理的 Pods 拥有持久化存储。即便 Pod 被删除，StatefulSet 为它分配的 PVC（持久卷声明）、PV（持久卷）会被保留下来，当 Pod 被创新创建后，Kubernetes 会为它找到之前挂载的 PVC，并为它挂载 PVC 对应的 Volume，从而可以获取到 Pod 被删除之前保存在 Volume 中的数据。



{{%children style="card" description="true" %}}





