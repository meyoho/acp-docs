+++
title = "任务记录"
description = "定时任务每执行一次，会创建一个任务记录（Job），您可查看任务记录的相关信息，掌握任务的执行情况。"
weight = 7
+++

定时任务每执行一次，会创建一个任务记录（Job），您可查看任务记录的相关信息，掌握任务的执行情况。


{{%children style="card" description="true" %}}





