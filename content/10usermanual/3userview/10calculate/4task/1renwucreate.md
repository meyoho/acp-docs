+++
title = "创建定时任务"
description = "在业务视图中，创建定时任务（Cronjob），并根据使用需要，选择任务执行方式、任务作业类型。"
weight = 1
+++

在业务视图中，创建定时任务（Cronjob），并根据使用需要，选择任务执行方式、任务作业类型。

支持通过填写可视化 UI 表单的方式，或自定义 YAML 编排文件的方式，创建定时任务。


**操作步骤**

1. 登录平台，切换至业务视图，进入待创建定时任务的命名空间。

1. 单击左导航栏中的 **计算** > **定时任务**，再单击 **创建定时任务**。

3. 在弹出的 **选择镜像** 窗口，单击选择 **方式**。

	**说明**：仅当同时订阅了本公司的 DevOps 产品，本平台对接了 DevOps 平台的镜像仓库时需要选择方式。

	* **输入**：地址格式为`仓库地址:镜像版本`，例如 ：`index.docker.io/library/ubuntu:latest`。<br>单击镜像地址输入框下方的镜像地址示例后的 ![](/img/input.png) 图标，可快速输入示例镜像地址。
	
	* **选择**：通过单击镜像仓库右侧的下拉选择框选择 DevOps 平台中的镜像仓库，并选择或输入镜像版本来选择要使用的镜像。 

2. 在创建定时任务页面，默认使用 **表单** 形式创建持久卷声明。配置定时任务参数。

	* 在 **名称** 框中，输入任务的名称。  
			名称支持小写英文字母、数字 0 ~ 9 和中横线，不支持以中横线结尾，字符数：1 ~ 32 个。

	* （非必填）在 **显示名称** 框中，输入任务的显示名称，支持中文字符。

3. 在 **定时配置** 区域，设置任务的触发执行方式等相关参数。首先在 **执行方式** 区域，选择 **定时触发** 或 **手动触发**。
	
	* 如果选择了 **手动触发**，您需要在每次决定执行任务的时间点，手动触发任务执行。
	
	* 如果选择了 **定时触发**，需要配置以下定时参数：  
		
		* 在 **触发规则** 区域，使用定时 Crontab 命令语法，定义任务触发执行的时间点。设置完成后，可以查看下次触发任务的时间。

			**注意**：此处输入的时间必须是 UTC 时间，时间转换规则和输入格式请参考：[如何设置定时任务的定时触发规则？]({{< relref "100faq/25.md" >}})  
		
		* 在 **定时并发策略** 区域，选择当前一个任务未执行完时，触发了下一次任务的并发处理策略。

	* 无论选择了何种触发执行方式，需要在 **最多保留任务历史数量** 区域，设置最多要保留的 **成功任务历史** 个数和 **失败任务历史** 个数，默认均为 20 个。一旦任务历史数量超过了 **最多保留任务历史数量**，系统将自动删除最早的任务历史。

4. 在 **任务配置** 区域，设置任务执行的相关参数。首先选择 **任务作业类型**，包括 **单次作业**、**并行作业** 和 **固定次数作业**。

	* **说明**：任务的作业是通过容器组实际完成的，您可以根据实际需求选择不同的 **任务作业类型**，相应创建一个或多个容器组执行任务。一个容器组从创建到完成业务（Succeeded or failed），即为一次作业过程；一个作业的容器组达到 Succeeded 状态，计算一次作业成功；一个作业的容器组达到 Failed 状态，计算一次作业失败。
	
	* **单次作业**：任务每次触发执行时，一次执行单个作业。

	* **并行作业**：任务每次触发执行时，可并行执行多个作业。

	* **固定次数作业**：任务每次触发执行时，可并行执行多个作业，并且需要固定次数的成功作业。

5. 在 **任务配置** 区域，根据选择的任务作业类型，继续设置任务执行的相关参数。

	* **失败重试次数**：即 backofflimit，无论选择何种任务作业类型，均需配置此项。执行作业的容器组达到 Failed 状态的次数，如果超过了设置的作业失败重试次数，任务被判定为失败。

	* **最大并行次数**：即 parallelisms，当任务作业类型为支持并行执行多个作业的 **并行作业** 和 **固定次数作业** 时，需要配置此项，设置最多可并行执行多少个作业（运行的容器组）。
	
	* **计划成功次数**：即 completions，仅当任务作业类型为 **固定次数作业** 时，需要配置此项，设置需要几次成功作业（达到 Succeeded 状态的容器组）。

	* **任务超时时间**：即 ActiveDeadlineSeconds，整个任务的超时时间，超过这个时间后，无论任务状态如何，任务判定为失败。


6. 在 **容器组** 区域，单击 **凭据** 下拉选择框，选择一个或多个用于拉取镜像的凭据。

6. 在 **容器** 区域，配置容器实例的基本信息：
	
	- 在 **名称** 框中，输入容器的名称，默认为镜像仓库名称。  
			名称支持小写英文字母、数字 0 ~ 9 和中横线，不支持以中横线开头结尾。字符数：1 ~ 32 个。

	- 在 **镜像地址** 框中，默认显示已选择的镜像。单击 **选择镜像**，在 **选择镜像** 窗口，可以重新选择镜像，参考 ***步骤 3***。

	- 在 **资源限制** 区域，输入容器的 **限制值**，限制容器实例运行过程中，最多可使用的节点计算资源值。默认为所在命名空间的 **默认值**，重新设置后的该值不能大于命名空间容器限额的 **最大值**。支持选择不同单位。
		

7. 单击 ![](/img/downrow.png) ，配置容器实例的高级参数。

	- 在 **启动命令** 和 **参数** 框中，单击 **添加**，根据需要，分别输入容器启动时要执行的自定义命令和命令参数。

		* **启动命令**：即 command，相当于 Dockerfile 中的 ENTRYPOINT 命令。如果没有指定启动命令，将使用容器镜像中的 ENTRYPOINT。
		
		* **参数**：即 args，相当于 Dockerfile 中的 CMD，负责提供启动命令的命令参数。如果没有指定参数，将使用容器镜像中的 CMD。

		**示例**：
		
		* 在容器启动时，执行 `top -b` 命令示例。  
		在 **启动命令** 区域，添加 `top` 和 `-b`，预览 YAML 如图所示；<br>![](/img/command01.png)  
		
			或在 **启动命令** 区域，添加 `top`，在 **参数** 区域，添加 `-b`，预览 YAML 如图所示。<br>![](/img/command02.png) 
		
		* 支持在启动命令和参数中引用环境变量。例如：已设置环境变量 `MESSAGE:Welcome!`，可在如下图所示的命令示例中引用环境变量。<br>![](/img/command03.png) 

		* 更多示例和说明可参考 [官方文档](https://kubernetes.io/docs/tasks/inject-data-application/define-command-argument-container)。

	- 在 **环境变量** 区域，设置容器的环境变量。<br>单击 **添加**，输入环境变量的键和对应的值；或单击 **添加引用**，在 **键** 栏中，输入环境变量的键；在 **值** 栏中，选择要引用的配置字典或保密字典，再选择要引用配置项的键，引用配置项的值作为这个环境变量的值。
		**注意**：通过环境变量添加相同的 key，会覆盖配置文件或镜像的相关配置。
	
	- 在 **配置引用** 框中，选择一个或多个要完整引用的配置字典。
	
	- 单击 ![](/img/add.png) 添加一条端口记录。<br>单击 **协议** 下拉选择框，选择端口的协议，可选 TCP、UDP。<br>在 **容器端口** 输入框中，输入容器端口号。<br>在 **主机端口** 输入框中，输入要映射的主机端口号。

	
	- 在 **日志文件** 区域，单击 **添加**，输入在容器中要收集保存的日志文件所在的目录路径，例如：`/var/log/`。

		**注意**：
		
		* 实现文件日志功能，需要所在集群部署日志服务。
		
		* 容器存储驱动为 overlay2 时，支持此功能；如果容器存储驱动为 devicemapper，还需要自行挂载 Emptydir 类型的存储卷（volume）到日志文件所在的目录路径。

	- 在 **排除日志文件** 区域，单击 **添加**，输入不需要收集保存的日志文件所在的目录路径，将它们排除，例如：`/var/log/aaa.log`。

8. 单击创建定时任务页面右上角的 **YAML**，可从表单模式切换到 YAML 模式，查看 YAML 或直接用 YAML 创建定时任务（适用于熟悉 Kubernetes 且可以熟练使用 YAML 的用户）。同时支持以下操作：
	
	* 单击 **导入**，选择已有的 YAML 文件内容作为编辑内容。

	* 单击 **导出**，将 YAML 导出保存成一个文件。 

	* 单击 **清空**，清空所有编辑内容，会同时清空表单中的编辑内容。
	
	* 单击 **恢复**，回退到上一次的编辑状态。	
		
	* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
	* 单击 **复制**，复制编辑内容。

	* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
	
	* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。
	
	* 单击 **显示更新**，更新的 YAML 内容会以不同颜色显示。 

9. 确认信息无误后，单击 **创建** 按钮。
