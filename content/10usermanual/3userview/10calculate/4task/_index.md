+++
title = "定时任务"
description = "定时任务（CronJob）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过平台创建一个 CronJob 来定期或重复运行一个非持续性的程序。例如：定时备份、定时清理、定时发送邮件等。"
weight = 4
+++

定时任务（CronJob）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过平台创建一个 CronJob 来定期或重复运行一个非持续性的程序。例如：定时备份、定时清理、定时发送邮件等。


平台中的定时任务除支持定时触发方式外，还支持手动触发方式，您可以在创建定时任务时，选择需要的执行方式。

* **定时触发**：您需要使用定时 Crontab 命令语法，定义任务触发执行的时间点，或如何周期性地运行。

* **手动触发**：选择该执行方式的任务，既不会定时执行，也不会在创建任务后立即执行。您需要在每次决定执行任务的时间点，手动触发任务执行。

{{%children style="card" description="true" %}}





