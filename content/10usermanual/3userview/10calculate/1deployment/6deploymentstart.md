+++
title = "开始部署"
description = "启动处于停止状态的部署。"
weight = 6

+++

启动处于停止状态的部署，将会重新为部署创建容器组实例（Pods）。

**操作步骤**

1. 登录平台，切换至业务视图，进入待开始部署的命名空间。

1. 单击左侧导航栏中的 **计算** > **部署**，进入部署列表页面。

2. 单击待开始部署的 ***名称***，进入部署的详情页面。

1. 单击 **开始** 按钮，并在弹出的确认提示框中单击 **确定** 按钮。

	部署的状态变为处理中则部署已经开始启动。



    

