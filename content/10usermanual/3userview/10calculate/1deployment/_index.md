+++
title = "部署"
description = "部署（Deployment）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过创建一个 Deployment 来运行一个程序。也可以在应用中创建多个 Deployment 作为应用下的计算组件来为应用提供特定的服务。"
weight = 1
+++

部署（Deployment）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过创建一个 Deployment 来运行一个程序。也可以在应用中创建多个 Deployment 作为应用下的计算组件来为无状态应用提供特定的服务。

Deployment 为 ReplicaSet（副本控制器） 和 Pod（容器组） 提供了声明式的更新。只需要在 Deployment 中描述了对 Replicas（Pods 数量） 和 Containers（容器）的期望状态，Deployment 控制器会将 Pod 和 ReplicaSet 的实际状态改变至期望状态。

**Deployment 的功能特点如下：**

* 可以通过创建一个 Deployment 来启动一个 ReplicaSet。ReplicaSet 会根据设定的副本数创建 Pods，并检查 Pods 的启动状态。

* 可以通过更新 Deployment 的 PodTemplateSpec 字段来声明 Pods 的新状态。声明 Pods 的新状态后，会创建一个新的 ReplicaSet，Deployment 会以可控的速率将 Pods 从旧的 ReplicaSet 迁移至新的 ReplicaSet。同时，ReplicaSet 会更新 Deployment 的版本。

* 如果当前 Deployment 的状态不稳定，将回滚至更早版本的 Deployment。每次回滚将会更新 Deployment 的版本。

* 可以通过扩容 Deployment 来满足更高的负载。

* 可以通过暂停 Deployment 来应用 PodTemplateSpec 的多个修复，然后重新上线。

* 可以将 Deployment 的状态作为判断上线是否停滞的指标。

* 可以通过 Deployment 清理不再需要的旧的 ReplicaSet。

{{%children style="card" description="true" %}}





