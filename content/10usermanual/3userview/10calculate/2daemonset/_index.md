+++
title = "守护进程集"
description = "守护进程集（DaemonSet）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过平台创建一个 DaemonSet 来运行一个程序。也可以在应用中创建多个 DaemonSet 作为应用下的计算组件来为应用提供特定的服务。"
weight = 2

+++

守护进程集（DaemonSet）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过平台创建一个 DaemonSet 来运行一个程序。也可以在应用中创建多个 DaemonSet 作为应用下的计算组件来为应用提供特定的服务。

**DaemonSet 的功能特点如下：**

- 能够保证所有或部分符合条件的主机节点上都能运行 Pod 的副本；

- 当有主机节点添加进集群时，DaemonSet 会自动为新增的主机节点上添加 DaemonSet 中的 Pods 副本；

- 当主机节点被从集群中移除时，主机节点上的 Pods 会被回收；

- 当 DaemonSet 被删除后，将会删除该 DaemonSet 创建的所有 Pods。

DaemonSet 的典型的使用场景如下：

* 在集群内的每个主机节点上运行一个集群存储守护程序，例如：glusterd、ceph。

* 在集群内的每个主机节点上运行一个日志收集守护程序，例如：fluentd 或 logstash。

* 在集群内的每个主机节点上运行一个节点监控守护程序，例如： [Prometheus Node Exporter](https://github.com/prometheus/node_exporter)、[Sysdig Agent](https://sysdigdocs.atlassian.net/wiki/spaces/Platform)、 `collectd`、 [Dynatrace OneAgent](https://www.dynatrace.com/technologies/kubernetes-monitoring/)、[AppDynamics Agent](https://docs.appdynamics.com/display/CLOUD/Container+Visibility+with+Kubernetes)、[Datadog agent](https://docs.datadoghq.com/agent/kubernetes/daemonset_setup/)、[New Relic agent](https://docs.newrelic.com/docs/integrations/kubernetes-integration/installation/kubernetes-installation-configuration)、Ganglia `gmond` 或 [Instana Agent](https://www.instana.com/supported-integrations/kubernetes-monitoring/)。


{{%children style="card" description="true" %}}





