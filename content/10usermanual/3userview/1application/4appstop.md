+++
title = "停止应用"
description = "停止运行应用。"
weight = 11
+++

停止处在运行中、处理中状态的应用。

**操作步骤**

1. 登录平台，切换至业务视图，进入待停止的应用所在的项目，并选择该应用所在的命名空间。

2. 单击 **应用**，进入应用列表页面。

3. 单击要停止的 ***应用名称***，进入在应用详情页面。

4. 单击 **停止** 按钮。

3. 在弹出的确认提示框中，单击 **确定** 按钮。

