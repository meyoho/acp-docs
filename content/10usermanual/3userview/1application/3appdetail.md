+++
title = "查看应用详情"
description = "在应用创建的过程中，以及完成后，支持查看应用详情，例如：组件、事件、日志和 YAML 编排文件。"
weight = 4
+++


在应用创建的过程中，以及完成后，支持查看应用详情，例如：容器组、事件、日志和 YAML 编排文件等。

**说明**：仅当应用所在集群部署了监控服务时，可查看应用的监控和告警信息。

**操作步骤**

1. 登录平台，切换至业务视图，进入待查看的应用所在的项目，并选择所在的命名空间。

2. 单击左侧导航栏中的 **应用管理** > **应用**，进入应用列表页面。

3. 单击待查看的 ***应用名称***，进入应用详情页面。

	在详情信息页面，支持查看应用的基本信息（状态、创建时间、显示名称、访问地址等）、组件（资源类型为 Deployment、DaemonSet 或 StatefulSet）信息、其他资源（Service、Ingress、HPA 等 ）信息。同时，支持以下操作：
	
	* **查看计算组件详情**：单击 ***计算组件名称***，可跳转至计算组件详情页面，参考 [查看计算组件详情]({{< relref "10usermanual/3userview/1application/6apppartdetail.md" >}})。
	
	* **扩缩容计算组件 Pod 数**：单击 Pods 示例图左右两侧的 ![](/img/addpod.png) 、![](/img/reducepod.png) 图标，可快速对部署进行扩容、缩容。

		![](/img/scalepods.png)
		
	* **更新镜像版本**：单击镜像右侧的  ![](/img/pencil.png) 图标，在弹出的更新镜像版本对话框中，输入 **镜像版本** 后单击 **确定** 按钮。

	* **更新容器限制**：单击 **资源限制** 右侧的 ![](/img/pencil.png) 图标，在弹出的更新容器限制对话框中，更新资源限制的值后单击 **确定** 按钮。

	* **EXEC 登录容器**：
			
		**注意**：仅支持 EXEC 登录已经运行起来的容器。
	
		1. 单击 **EXEC**，在下拉列表中，选择要登录的容器组。  
		支持选择 **全部容器组**，同时登录所有容器。
		
		2. 在 **EXEC** 窗口，输入 EXEC 登录时要执行的命令，默认为 `/bin/sh`。

		3. 单击 **确定** 按钮，打开 Web 控制台窗口，执行命令行操作。
	
	* **查看容器日志**：单击 **日志**，在下拉列表中，选择要查看日志的容器组。会跳转至容器日志页面。
	
	* **查看其他资源详情**：在 **其他资源** 区域，单击 ***资源名称***，可跳转至资源的详情页面，可查看资源的详细信息。


6. 单击 **YAML** 页签，在 **YAML（只读）** 区域，查看应用当前的编排文件详情，同时支持以下操作：
	
	* 单击 **导出**，将 YAML 导出保存成一个文件。 
		
	* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
	* 单击 **复制**，复制编辑内容。

	* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
	
	* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。

5. 单击 **容器组** 页签，查看应用的所有容器组。
	
	支持查看容器组的 **名称**、**状态**（运行中、处理中、失败、未知）、**重启次数**（Pod 状态异常时，重启的次数）、**容器组 IP**、**创建时间**，同时，支持以下操作：
	
	* **查看容器组的 YAML 文件**：单击待查看 YAML 文件的 ***容器组名称*** ，在弹出的对话框中可查看容器组的 **YAML（只读）**	文件内容。同时，可对 YAML 文件执行以下操作：
		
		* 单击 **导出**，将 YAML 导出保存成一个文件。 
		
		* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
		* 单击 **复制**，复制编辑内容。
		
		* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
		
		* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。
	
	* **查看容器的日志**：单击待查看日志容器组记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **查看日志** > ***容器名称***，将跳转至容器日志页面。
	
	* **EXEC 登录容器**：单击待查看日志容器组记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **EXEC** > ***容器名称***，在 EXEC 对话框中，输入 EXEC 登录时要执行的命令，默认为 `/bin/sh`。<br>单击 **确认** 按钮后可打开 Web 控制台窗口，执行命令行操作。
	
	* **删除容器组**：单击待查看日志容器组记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **删除**，在弹出的提示框中单击 **确定** 按钮。
	
	* **刷新容器组列表**：单击列表右上角的 ![](/img/refresh.png) 按钮，可刷新容器组列表。



8. 单击 **拓扑** 页签，查看应用的组件结构拓扑图。


7. 单击 **历史版本** 页签，查看通过界面更新应用后自动生成的应用历史版本信息。

	* 单击 ***历史版本名称***，可查看应用的历史版本的 YAML（只读）文件；单击 **回滚至该版本** 按钮，可回滚应用至相应历史版本。
	
	* 单击历史版本记录右侧的 **回滚至该版本** 按钮，可回滚应用至相应历史版本。

7. 单击 **日志** 页签，查看应用容器实例运行时输入的容器日志。

	* 支持通过下拉选择框选择 **容器组** 和 **容器** 来过滤日志。
	
	* 支持在查看时，选择自动更新容器日志。
	
9. 单击 **事件** 页签，查看指定时间范围内与当前应用相关的所有事件信息。
	
10. 单击 **监控** 页签，支持查看一段时间内的应用监控详情。
	
	**注意**：仅当应用所在集群部署了监控服务时，该页签可见。
	
	支持查看 CPU 和内存的利用率、网络发送字节和接收字节，并可以按照提供的时间范围或自定义时间去查看监控详情。在折线图上，移动鼠标到每个点上，可以查看具体数值和单位。  
	
	查看应用监控时，支持修改数据聚合方式（平均值、最大值、最小值）;支持设置要查看的时间范围（默认为过去 30 分钟）。
	
11. 单击 **告警** 页签，可查看应用下所有的计算组件告警列表。

	**注意**：仅当应用所在集群部署了监控服务时，该页签可见。

	在应用的告警列表页面，可更新和删除计算组件的告警。
	

	在应用的告警列表页面可查看告警的以下信息，同时，支持更新和删除计算组件的告警。
	
	|  参数  |  说明  |
	| --- | --- |
	|  **名称**  |   告警的名称。   |
	|  **计算组件**  |  告警所属计算组件的名称。 |
	| **状态**    |  告警的状态。包括：<br>正常：未触发告警。<br>告警：已触发告警并发送。<br>处理中：已达告警阈值，即将触发告警的中间状态。 |
	| **指令**    |  通知的名称，通知名称过多时，隐藏部分名称，单击 ![](/img/003points.png) 图标可展开全部。 |
	| **描述**    |  告警的描述信息。 |
	| **创建时间**    | 告警的创建时间。 |
	| **操作**    | **更新告警**：单击指定的告警记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **更新**，更新告警的配置。<br>**删除告警**：单击指定的告警记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **删除**，删除告警。<br>**查看告警详情**：单击 ***告警名称***，进入告警详情页面，可查看告警的详情或执行更新和删除告警操作。<br>**进入计算组件详情页**：单击 ***计算组件名称***，进入计算组件详情页面。 |

	
	**说明**：如要为具体的计算组件创建告警，需进入相应的计算组件详情页，具体操作请参考  [为应用下的计算组件创建告警]({{< relref "10usermanual/3userview/60workloadalarm/2appworkloadalarmcreate.md" >}})。


   ​	
