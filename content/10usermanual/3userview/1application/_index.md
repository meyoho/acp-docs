+++
title = "应用"
description = "应用（Application）是本平台中的自定义 Kubernetes 资源，由一个或多个关联的计算组件构成整体业务应用，支持通过 UI 编辑模式或 YAML 编排文件创建，运行在开发、测试或生产环境中。"
weight = 1
+++

应用（Application）是本平台中的自定义 Kubernetes 资源，由一个或多个关联的计算组件构成整体业务应用，支持通过 UI 编辑模式或 YAML 编排文件创建，运行在开发、测试或生产环境中。

计算组件支持根据不同的部署和使用需求，选择对应的部署模式：Deployment、DaemonSet 和 StatefulSet。计算组件基于 Docker 镜像创建容器实例，并支持管理存储、健康检查、更新策略、网络等配置。

创建应用后，支持对应用进行一系列的生命周期管理操作，例如：启动、停止、更新和删除应用。

{{%children style="card" description="true" %}}





