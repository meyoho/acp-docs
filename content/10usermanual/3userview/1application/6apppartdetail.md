+++
title = "查看计算组件详情"
description = "在应用创建的过程中，以及完成后，都可以查看计算组件详情，例如：容器实例、访问地址、事件、监控、日志、YAML 编排文件、组件配置、容器配置。"
weight = 20
+++

在应用创建的过程中，以及完成后，都可以查看计算组件详情，例如：容器实例、访问地址、事件、监控、日志、YAML 编排文件、组件配置、容器配置。

**说明**：仅当计算组件所在集群部署了监控服务时，可查看计算组件的监控和告警信息。

**操作步骤**

1. 登录平台，切换至业务视图，进入待查看的计算组件所在的项目，并选择所在的命名空间。

2. 单击 **应用**，在应用列表页面，可以通过以下两种方式进入应用详情页：
   
   - 找到要查看计算组件所属的应用名称。单击操作栏中的 ![](/img/003point.png)，再单击 **查看**。
   
   - 单击要查看计算组件所属的 ***应用名称***。

3. 单击 **详情信息** 页签，在详情信息页面，查看计算组件（资源类型为 Deployment、DaemonSet 或 StatefulSet），找到要查看详情的计算组件。单击 ***计算组件名称***。

4. 在计算组件详情页，单击 **详情信息** 页签，查看计算组件的基本信息，容器详情和关联的内部路由信息。

	* **部署模式**：包括 **Deployment**、**DaemonSet** 和 **StatefulSet**。
		
		- **Deployment**：创建的计算组件没有特殊的状态要求。		
		- **DaemonSet**：每个可用的计算节点上运行一个守护进程，并通过选择主机标签部署在指定的主机上。实例数量一般与主机数量相等。
		
		- **StatefulSet**：创建的计算组件有状态要求。例如：实现网络的唯一标识、有序创建、更新和删除。

	* **容器组标签**：计算组件下容器组实例的标签（label）。单击 ![](/img/pencil.png) 图标，在弹出的 **更新标签** 对话框中，可添加或减少自定义标签。
	
	* **状态**：计算组件的状态信息，包括运行中、已停止和处理中。

		* **运行中**：正常运行的容器组数量与设置的实例数量相等。
		
		* **已停止**：有计划停止了所有容器组，相当于设置实例数量为 0。

		* **处理中**：运行中状态和已停止状态的中间状态，包括其它情形。

	* **扩缩容配置**：通过 HPA（HorizontalPodAutoscaler）实现自动弹性伸缩时，设置的实例数量范围，包括实例最小值和实例最大值。
	
	* **主机选择器**：调度计算组件到指定节点的标签信息。

	* **访问地址**：组件的访问地址。

	* **镜像**：创建计算组件的容器时使用的镜像。单击 ![](/img/pencil.png) 图标，在弹出的 **更新镜像版本** 对话框中，可修改镜像版本。
	
	* **容器大小**：容器实例创建时，最多使用的 CPU 和内存资源。单击 ![](/img/pencil.png) 图标，在弹出的 **更新容器大小** 对话框中，可更新容器的 CPU 和内存大小。

	* **启动命令**：容器启动时执行的命令。
	
	* **参数**：容器启动时执行命令的参数。

	* **内部路由**：与计算组件相关联的内部路由信息。
	
		* **类型**：Kubernetes Service 的类型。包括仅用于集群内部发现的 **HTTP（ClusterIP）**；用于暴露外部访问端口的 **TCP（NodePort）**；不具有负载均衡功能或独立 IP 的 **Headless**。
	
		* **服务端口**：即 Port，Kubernetes Service 在集群内部暴露的服务端口号。
	
		* **容器端口**：即 targetPort，容器提供服务的端口。

		* **协议**：选择要使用的协议名称。

	* **添加自动扩缩容**：单击 **添加自动扩缩容** 按钮，在弹出的 **添加自动扩缩容** 对话框中，输入 **最大实例数**、**最小实例数**、**CPU 利用率阈值** 后单击 **确定**。
		* **最大实例数**：弹性伸缩过程中容器实例数量的最大值。达到最大值时，实例数量不再增加。
		
		* **最小实例数**：弹性伸缩过程中容器实例数量的最小值。达到最小值时，实例数量不再减少。
		
		* **CPU 利用率阈值**：CPU 利用率的阈值，根据该阈值判断是否对组件进行自动扩缩容。CPU 利用率为“实际使用值/请求值”，当 CPU 利用率超过该阈值，则进行自动扩容；当 CPU 利用率低于该阈值一定比例后，进行自动缩容。


5. 单击 **容器组** 页签，查看计算组件的所有容器组。
	
	支持查看容器组的 **名称**、**状态**（运行中、处理中、失败、未知）、**重启次数**（Pod 状态异常时，重启的次数）、**容器组IP**、**创建时间**，同时，支持以下操作：
	
	* **查看容器组的 YAML 文件**：单击待查看 YAML 文件的 ***容器组名称*** ，在弹出的对话框中可查看容器组的 **YAML（只读）**	文件内容。同时，可对 YAML 文件执行以下操作：
		
		* 单击 **导出**，将 YAML 导出保存成一个文件。 
		
		* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
		* 单击 **复制**，复制编辑内容。
		
		* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
		
		* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。
	
	* **查看容器的日志**：单击待查看日志容器组记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **查看日志** > ***容器名称***，将跳转至容器日志页面。
	
	* **EXEC 登录容器**：单击待查看日志容器组记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **EXEC** > ***容器名称***，在 EXEC 对话框中，输入 EXEC 登录时要执行的命令，默认为 `/bin/sh`。<br>单击 **确认** 按钮后可打开 Web 控制台窗口，执行命令行操作。
	
	* **删除容器组**：单击待查看日志容器组记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **删除**，在弹出的提示框中单击 **确定** 按钮。
	
	* **刷新容器组列表**：单击列表右上角的 ![](/img/refresh.png) 按钮，可刷新容器组列表。

6. 单击 **YAML** 页签，在 **YAML（只读）** 区域，查看计算组件当前的编排文件详情，同时支持以下操作：
	
	* 单击 **导出**，将 YAML 导出保存成一个文件。 
		
	* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
	* 单击 **复制**，复制编辑内容。

	* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
	
	* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。

8. 单击 **配置** 页签，查看容器实例运行时使用的所有环境变量键值对。

	* **更新环境变量**：单击 **更新** 按钮，在弹出的 **更新环境变量** 对话框中，单击 **添加**，添加环境变量的键值对；或单击 **添加引用**，在 **键** 栏中，输入环境变量的键；在 **值** 栏中，选择要引用的配置字典或保密字典，再选择要引用配置项的键，引用配置项的值作为这个环境变量的值。

		**注意**：通过环境变量添加相同的 key，会覆盖配置文件或镜像的相关配置。
	
	* **更新配置引用**：单击 **更新** 按钮，在弹出的 **更新配置引用** 对话框中，通过单击 **配置引用** 下拉选择框中的配置文件，更新引用完整的配置文件（配置字典或保密字典）。

7. 单击 **日志** 页签，查看容器实例运行时的标准日志。可通过 **容器组名称** 和 **容器名称** 进行过滤。支持在查看时，选择自动更新容器日志，查找日志关键词。

8. 单击 **监控** 页签，查看一段时间内的计算组件的监控详情。

	**注意**：仅当计算组件所在集群部署了监控服务时，该页签可见。
	
	 支持查看 CPU 和内存的利用率、网络发送字节和接收字节，并可以按照提供的时间范围或自定义时间去查看监控详情。在折线图上，移动鼠标到每个点上，可以查看具体数值和单位。  
	
	查看计算组件监控时，支持选择数据分组方式（计算组件、Pod、容器）；支持修改数据聚合方式（平均值、最大值、最小值）；设置要查看的时间范围（默认为过去 30 分钟）。


9. 单击 **告警** 页签，可查看计算组件告警列表。

	**注意**：仅当计算组件所在集群部署了监控服务时，该页签可见。
	
	在计算组件的告警列表页面可查看告警的以下信息，同时，支持更新和删除计算组件的告警。
	
	<style>table th:first-of-type { width: 10%;}</style>
	
	|  参数  |  说明  |
	| --- | --- |
	|  **名称**  |   告警的名称。   |
	|  **计算组件**  |  告警所属计算组件的名称。 |
	| **状态**    |  告警的状态。包括：<br>正常：未触发告警。<br>告警：已触发告警并发送。<br>处理中：已达告警阈值，即将触发告警的中间状态。 |
	| **指令**    |  通知的名称，通知名称过多时，隐藏部分名称，单击 ![](/img/003points.png) 图标可展开全部。 |
	| **描述**    |  告警的描述信息。 |
	| **创建时间**    | 告警的创建时间。 |
	| **操作**    | **更新告警**：单击指定的告警记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **更新**，更新告警的配置。<br>**删除告警**：单击指定的告警记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中选择 **删除**，删除告警。<br>**查看告警详情**：单击 ***告警名称***，进入告警详情页面，可查看告警的详情或执行更新和删除告警操作。 |
	
	同时，支持创建计算组件的监控指标告警，并管理相关告警规则。参考 [为应用下的计算组件创建告警]({{< relref "10usermanual/3userview/60workloadalarm/2appworkloadalarmcreate.md" >}})。

		
	
