+++
title = "从 YAML 创建应用"
description = "通过导入已有的或编辑新的 YAML 编排文件来创建应用，更具便捷性、灵活性和可维护性。"
weight = 3
+++

通过导入已有的或编辑新的 YAML 编排文件来创建应用，更具便捷性、灵活性和可维护性。


**操作步骤**

1. 登录平台，切换至业务视图。进入待创建应用的项目，并选择要创建应用的命名空间。

3. 单击 **应用**，在应用列表页面，单击 **创建应用** 右侧的箭头，再单击 **从 YAML 创建**。

2. 在从 YAML 创建应用页面，在 **名称** 框中，输入应用名称，默认为选择的镜像仓库名称。<br>名称支持小写英文字母、数字 0 ~ 9 和中横线，不支持以中横线结尾，字符数：1 ~ 32 个。
	
3. （非必填）在 **显示名称** 框中，输入应用显示名称，支持中文字符，默认为选择的镜像仓库名称。如未输入，默认显示应用名称。

6. 在 **YAML（读写）** 区域，输入应用的 YAML 编排文件内容，同时支持以下操作：
	
	* 单击 **导入**，导入已有的 YAML 文件内容作为编辑内容。

	* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 
	
	* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。

	* 单击 **YAML 样例** 区域的 **写入**，参考提供的 YAML 样例创建应用。    
			
	* 单击 **YAML 样例** 区域的 **查看**，在 **YAML 样例** 窗口，查看提供的 YAML 样例的具体内容。 
			
6. 单击 **创建**。<br>在应用详情页面，查看应用的状态信息，当创建成功后，应用的状态会变成 **运行中**。
