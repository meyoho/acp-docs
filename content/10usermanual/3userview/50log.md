+++
title = "查看应用日志"
description = "根据时间范围和查询条件，搜索标准输出（stdout）日志。"
weight = 50
+++

应用日志记录了当前命名空间中所有应用的运行情况。

<img src="/img/applog.png" width = "860" />

支持通过日志查询条件筛选指定时间范围内的日志，并通过柱状图显示当前查询时间范围内的日志总条数和不同时间节点的日志条数，如下图所示。

<img src="/img/logggregations.png" width = "860" />

**操作步骤**

1. 登录平台，切换至业务视图，单击做导航栏中的 **日志**。  
		默认显示过去 30 分钟内，当前命名空间中的应用日志，以柱状图的形式呈现，并按照应用名称、容器实例、日志输出源的类别以不同的颜色显示了详情。支持单击某一个类别后，添加这个类别为查询条件。

3. 在 **时间范围** 框中，下拉选择过去的某个时段，也可以选择 **自定义时间**，自定义日期和具体时间。<br>确定了时间范围后，日志的柱状图和详情显示了选定时间段的日志。最多支持查看过去 7 天的日志。

4. 在 **日志查询条件** 框中，输入应用名称（application）、以及关键字过滤搜索。  
		<br>单击 **日志查询条件** 框中的空白处，默认显示了所有应用名称（application）的查询条件，并以 `类别：类别条件` 的形式显示。应用（application）的搜索条件为 `application:application_name`。  
		<br>不同类别之间是 and 关系，同类别之间的多个条件之间是 or 的关系。例如：查询条件为 `application:app1`、`application:app2`、 `keyword`，搜索结果这 2 个应用（app1 和 app2）中带有 keyword 的日志。  
		
	**注意**：不支持模糊搜索。

5. 单击 **搜索**。

   * 单击柱状图的某个柱体，查看这个柱状图和下一个柱状图之间的时间范围内的日志。

   * 如果日志条数很多，在右下角的页数框中，输入页数，跳转到对应页数的日志。

1. （可选）单击日志查询记录左侧的 “+” 图标，可查询日志上下文。<br>可展示当前日志打印时间前、后各 5 条日志，便于运维人员更详细的了解资源产生当前日志的原因。<br>进一步单击上下文记录左侧的上、下箭头，可查询更多的上下文信息。<br>单击 “-” 图标，可隐藏日志上下文。

	<img src="/img/logcontext.png" width = "660" />