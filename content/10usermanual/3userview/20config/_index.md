+++
title = "配置"
description = ""
weight = 20
+++

平台全面对接 Kubernetes，支持通过配置字典（ConfigMap）保存配置数据，通过保密字典保存用户需要保密的敏感信息或配置数据。

当创建应用时，通过环境变量可将完整的配置字典、单独的配置项、完整的保密字典引入容器中，即保证了容器化应用的可移植性，又保障了用户敏感数据的安全。


{{%children style="card" description="true" %}}

