+++
title = "保密字典"
description = "若需要在 Kubernetes 集群中储存需要保密的敏感信息或配置，例如：用户密码、OAuth 令牌、SSH 私钥、访问镜像仓库的认证信息等，建议优先使用保密字典（Kubernetes Secret）储存。"
weight = 4
alwaysopen = false
+++

若需要在 Kubernetes 集群中储存需要保密的敏感信息或配置，例如：用户密码、OAuth 令牌、SSH 私钥、访问镜像仓库的认证信息等，建议优先使用保密字典（Kubernetes Secret）储存。保密字典具有以下特点：

* 相比于使用明文文本的配置字典（Kubernetes ConfigMap），保密字典的安全性更好（使用 Base64 编码格式存储敏感信息），且具有控制敏感信息使用，减少信息暴露风险的能力。

* 相比于将敏感信息储存于 Pod 定义文件或容器镜像中，使用保密字典更加安全、灵活，便于控制修改。

## 支持创建的保密字典类型

平台支持通过 UI 编辑模式创建的保密字典类型如下所述。

**Opaque**：一般类型的保密字典，可用于存储密码、密钥等信息，以 Map 键值对存储数据。

**TLS**：用于存储 TLS 协议证书和私钥认证信息。例如：HTTPS 证书凭据。

**SSH 认证**：即 ssh-auth，用于存储通过 SSH 协议传输数据的认证信息。

**用户名/密码**：即 basic-auth，用于存储用户名和密码认证信息。

**镜像服务**：即 dockerconfigjson，用于存储私有镜像仓库（Docker Registry）的 JSON 鉴权串。

## 使用保密字典的方式

使用 UI 编辑模式创建应用的计算组件时，在 **配置引用** 框中，选择要引用到容器实例的保密字典，将保密字典的所有配置项作为容器实例的环境变量。

使用 UI 编辑模式创建应用的计算组件时，在 **环境变量** 区域，将保密字典中的配置项，引用为容器实例的环境变量。

使用 YAML 创建应用时，可参考 [Kubernetes 官方文档](https://kubernetes.io/docs/concepts/configuration/secret/#using-secrets)，使用保密字典（适用于熟悉 Kubernetes 且可以熟练使用 YAML 的用户）。



{{%children style="card" description="true" %}}

