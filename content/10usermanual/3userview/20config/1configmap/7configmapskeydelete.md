+++
title = "删除配置项"
description = "删除已添加的配置项。"
weight = 7
+++

删除已添加的配置项。

**操作步骤**

1. 登录平台，切换至业务视图，进入要创建配置字典的命名空间。

3. 单击 **配置** > **配置字典**，在配置字典列表页面，找到要删除配置项的配置，支持根据配置字典名称进行搜索。

4. 单击 ***配置字典名称***，进入配置字典详情页面，在 **配置项** 区域，找到要更新的配置项。支持以键的名称作为关键字进行搜索。

5. 单击右侧的 ![](/img/00delete.png) 图标，在弹出的更新对话框中更新配置项的值。

6. 在弹出的确认提示框中，单击 **确定**。<br>删除成功后，对于用环境变量引用了这个配置字典，或引用了该配置项的计算组件，发生重建 Pod 后，可能因找不到引用的配置项，影响 Pod 的正常创建。
