+++
title = "创建联邦应用"
description = "创建一个新的联邦应用。"
weight = 2
+++


在联邦集群的控制集群的联邦命名空间中，创建一个新的联邦应用。

**前提条件**

需要进入控制集群下的联邦命名空间。

**操作步骤**

1. 登录平台，切换至业务视图，进入控制集群的联邦命名空间。

2. 单击左侧导航栏中的 **应用** > **联邦应用** ，进入联邦应用列表页面。

3. 单击 **创建联邦应用** 按钮，进入创建联邦应用页面，支持通过表单或 YAML 的形式创建联邦应用，默认以表单的形式创建。

	**说明**：单击右上角的 **表单**、**YAML** 按钮，可根据喜好切换创建方式。<br>当您选择 **YAML** 方式创建时，在 **YAML（读写）** 区域，输入联邦应用的 YAML 编排文件内容，同时支持以下操作：

	* 单击 **导入**，导入已有的 YAML 文件内容作为编辑内容。
	
	* 单击 **导出**，将 YAML 导出保存成一个文件。
	
	* 单击 **清空**，清空所有编辑内容，会同时清空表单中的编辑内容。

	* 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。  
		
	* 单击 **复制**，复制编辑内容。

	* 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。 

	* 单击 **全屏**，全屏查看内容；单击 **退出全屏**，退出全屏模式。

2. 在 **创建联邦应用** 区域，配置联邦应用的基本信息。

	* 在 **名称** 输入框中，输入联邦应用的名称。<br>支持输入 `a-z` 、`0-9`、`-`，以 `a-z` 、`0-9` 开头或结尾，且总长度不超过 64 个字符。
	
	* （非必填）在 **显示名称** 输入框中，输入联邦应用的显示名称，支持中文字符。
	
1. 在 **资源** 区域，配置联邦应用的联邦资源信息。
	
	* 单击 **添加部署**，可为联邦应用添加一个部署，详细操作请参考 [创建部署]({{< relref "10usermanual/3userview/10calculate/1deployment/2deploymentcreate.md" >}})。
	
	* 单击 **其他** 下拉选择按钮，可选择添加 **内部路由**、**有状态副本集**、**配置字典**、**保密字典**，具体操作请参考：

		*  [创建有状态副本集]({{< relref "10usermanual/3userview/10calculate/3statefulset/2statefulsetcreate.md" >}})
		
		* [创建内部路由]({{< relref "10usermanual/3userview/30netuserview/1service/1servicecreate.md" >}})

			**说明**：联邦应用下创建的内部路需要选择 **工作负载** 以关联联邦应用下已有的部署或有状态副本集。
			
		* [创建配置字典]({{< relref "10usermanual/3userview/20config/1configmap/1configmapscreate.md" >}})
		
		* [创建保密字典]({{< relref "10usermanual/3userview/20config/2secret/1secretcreate.md" >}}) 
	
1. 在 **差异化** 区域，单击 **添加差异化** 按钮，在弹出的 **添加差异化** 对话框中，为不同的联邦集群成员下的联邦应用提供差异化能力。

	* 更新联邦应用的 **显示名称**。
	
		1.  单击 **资源类型** 下拉选择框，并选择 **应用**。
		2. 在 **差异化** 区域	，单击 **添加**，添加一条配置记录。支持添加多条配置记录，不支持为同一资源添加多条相同的配置。
		3. 单击 **集群** 下方下拉选择框，选择待更新显示名称的联邦应用所在集群的名称。
		4. 单击 **参数** 下方下拉选择框，选择 **显示名称**。
		5. 在 **值** 输入框中，输入待更新的显示名称。

	* 更新联邦应用下的部署或有状态副本集的 **实例数**、**资源限制（CPU、内存）**。

		1.  单击 **资源类型** 下拉选择框，并选择 **部署/有状态副本集**。
		2. 单击 **资源** 下拉选择框，选择联邦应用下已有的部署/有状态副本集名称。
		2. 在 **差异化** 区域	，单击 **添加**，添加一条配置记录。支持添加多条配置记录，不支持为同一资源添加多条相同的配置。
		3. 单击 **集群** 下方下拉选择框，选择待更新显示名称的联邦应用所在集群的名称。
		4. 单击 **参数** 下方下拉选择框，选择 **实例数量/资源限制-CPU/资源限制-内存**。
		5. 在 **值** 输入框中，输入与参数对应的 **实例数量（个）/资源限制-CPU（m）/资源限制-内存（Mi）** 的值。

	


12. 完成所有配置后，单击 **创建**，联邦应用创建完成并进入联邦应用详情页面。  

	联邦应用创建成功后，可在当前命名空间以及所有同名的联邦命名空间的资源列表中查看联邦资源，例如：应用列表、部署列表、有状态副本集列表、内部路由列表、配置字典列表等。







