+++
title = "联邦应用（Alpha）"
description = "支持在联邦集群的所有成员集群下，跨集群部署和管理相同的应用，实现“两地三中心”容灾备份解决方案。"
weight = 5
+++

支持在联邦集群的所有成员集群下，跨集群部署和管理相同的应用，实现“两地三中心”容灾备份解决方案。

可在联邦集群的 **控制集群** 下的 **联邦命名空间** 中创建、管理联邦应用。

创建联邦应用后，会在联邦集群的所有成员集群的联邦命名空间下，创建一个同名的应用以及应用下的其他联邦资源，例如：部署、配置字典、内部路由、有状态副本集等。

**注意**：仅支持在控制集群下的联邦命名空间中创建、更新联邦应用，而非控制集群下联邦命名空间中的联邦应用及应用下联邦资源仅支持查看。

**说明**：

* 支持在联邦应用所在的命名空间的应用或其他资源的列表页、详情页中，查看联邦应用及其他的联邦资源的列表信息和详情信息。通过 ![](/img/00fed.png) 图标可区分联邦资源和非联邦资源。

* 不支持直接更新联邦资源，可通过更新控制集群下联邦应用的方式更新联邦资源。

<img src="/img/fedcluster.png" width = "460" />

**参考内容**：

[创建联邦集群]({{< relref "10usermanual/3userview/5fedapp/1fedappcreateimage.md" >}})

[创建项目]({{< relref "10usermanual/projectmanagement/project/1addproject.md" >}})

[添加项目关联集群]({{< relref "10usermanual/projectmanagement/project/5addcluster.md" >}})

[创建联邦命名空间]({{< relref "10usermanual/4adminview/1clusterall/2fedcluster/2fedclustercreate.md" >}})




{{%children style="card" description="true" %}}





