+++
title = "告警"
description = "平台管理员可通过平台管理集群、主机节点的指标告警，同时，可统一管理平台的告警模板，方便为集群、主机节点、计算组件创建告警。"
weight = 70
alwaysopen = false
+++

平台管理员可通过平台管理集群、主机节点的指标告警。

同时，可统一管理平台的告警模板，方便为集群、主机节点、计算组件创建告警。


{{%children style="card" description="true" %}}





