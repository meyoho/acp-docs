+++
title = "Kube-OVN 子网"
description = "当集群的网络模式为 Kube-OVN 时，支持创建并管理子网。"
weight = 20
alwaysopen = false
+++

当集群的网络模式为 Kube-OVN 时，支持创建并管理子网。


{{%children style="card" description="true" %}}



