+++
title = "子网（Alpha）"
description = "支持为使用 Kube-OVN、Calico 容器网络模式的集群划分并管理子网资源，实施网段隔离。当集群的网络模式为其它模式时，不支持创建子网。"
weight = 3
alwaysopen = false
+++

支持为使用 Kube-OVN、Calico 容器网络模式的集群划分并管理子网资源，实施网段隔离。当集群的网络模式为其它模式时，不支持创建子网。


{{%children style="card" description="true" %}}



