+++
title = "网络"
description = "在平台的管理视图中，平台管理员可通过平台管理集群的网络资源，包括：负载均衡、域名。"
weight = 2
alwaysopen = false
+++

在平台的管理视图中，平台管理员可通过平台管理集群的网络资源，例如：负载均衡、域名、子网。

{{%children style="card" description="true" %}}



