+++
title = "通知模板"
description = "通过配置通知模板，通过脚本可以灵活定义通知的内容及格式。"
weight = 30
alwaysopen = false
+++

通过配置通知模板，通过脚本可以灵活定义通知的内容及格式。

自定义通知的内容和格式，便于收到通知的用户能够快速获取有效信息。


{{%children style="card" description="true" %}}





