+++
title = "存储类"
description = "持久卷（PersistentVolume,PV）是表示与集群中后端存储卷映射关系的 Kubernetes 资源，是集群相关资源，负责将实际的存储资源抽象化，成为集群的存储基础设施。"
weight = 2
alwaysopen = false
+++

存储类（StorageClass）是一种用于描述 Kubernetes 集群可用的存储资源“类别”的 Kubernetes 资源，是集群相关资源。管理员可以根据存储类型、服务质量级别、备份策略或管理员确定的任意策略，通过存储类区分存储资源的类别。

普通用户可以为持久卷声明（PersistantVolumeClaim，PVC）关联管理员设置的存储类，快速设置要请求的存储类型。


<img src="/img/storageclass.png" width = "860" />

{{%children style="card" description="true" %}}


## 存储类功能特点

基于存储类，可实现按需动态创建持久卷（PersistentVolume，PV），管理员无需手工创建。

快速定义集群提供的存储资源类型，并支持设置默认存储类，方便普通用户进行选择关联，节约管理员的时间。

通过存储类，可为集群中的持久卷对接不同类型的后端存储。

## 存储类与后端存储

查看存储类可定义对接的后端存储类型，可参考 Kubernetes 官方文档。平台默认支持 Ceph 分布式存储方案。