+++
title = "存储"
description = "在平台的管理视图中，平台管理员可通过平台管理集群的存储资源，包括：持久卷、存储类。"
weight = 3
alwaysopen = false
+++

在平台的管理视图中，平台管理员可通过平台管理集群的存储资源，包括：持久卷、存储类。

{{%children style="card" description="true" %}}



