+++
title = "专有名词对照表"
description = "以下介绍本文档中出现的专有名词、术语以及缩略语。"
weight = 1000
+++

以下介绍本文档中出现的专有名词、术语以及缩略语。

<style>table th:first-of-type { width: 20%;}</style> 

<style>table th:nth-of-type(2) { width: 20%;}</style>

<style>table th:nth-of-type(3) { width: 10%;}</style>

<style>table th:nth-of-type(4) { width: 50%;}</style>


| 中文全称           | 英文全称                              | 缩略语 | 说明                                                         |
| ------------------ | ------------------------------------- | ------ | ------------------------------------------------------------ |
| 集群               | Cluster                               | -      | Kubernetes 集群，管理 Kubernetes 容器平台中，供容器运行的基础设施集群，关联了若干主机节点、负载均衡、专有网络等资源。<br>单点集群：只有一个 Master 节点的集群；<br>高可用集群：至少具有 3 个 Master 节点的集群。 |
| 联邦集群               | Cluster Federation       | -      | 平台支持将多个集群联邦后，进行统一管理。联邦集群下资源可以根据部署情况进行联邦化改造，为企业级容灾提供了新的解决方案。 |
| 主机节点           | Node                                  | -      | 集群下的主机节点，分为控制节点（Master）和计算节点（Node）。<br>控制节点负责运行集群中的 kube-apiserver、kube-scheduler、kube-controller-manager、etcd 和容器网络以及平台的部分管理组件。<br>计算节点是 Kubernetes 集群中承担工作负载的节点，可以是虚拟机也可以是物理机。计算节点承担实际的 Pod 调度以及与管理节点的通信等。 |
| 项目               | Project                               | -      | 平台的项目（租户）之间可以灵活的划分出独立且相互隔离的资源空间，每个项目都拥有独立的项目环境，能够代表企业中不同的子公司、部门或项目组。通过项目管理，能够轻松实现项目组之间的资源隔离、租户内的配额管理。 |
| 命名空间           | Namespace                             | -      | Kubernetes Namespace，在平台上是项目下相互隔离的更小的资源空间，也是用户实现作业生产的工作区间。一个项目下可以创建多个命名空间，可占用的资源配额总和不能超过项目配额。命名空间更细粒度的划分了资源配额的同时，还限制了命名空间下容器的大小（CPU、内存），有效的提升了资源利用率。 |
| 镜像               | Image                                 | -      | Docker 镜像是容器应用打包的标准格式，在部署容器化应用时可以指定镜像，镜像可以来自于 Docker Hub，本公司的 DevOps 平台镜像仓库，或者用户的私有 Registry。镜像 ID 可以由镜像所在仓库 URI 和镜像 Tag（缺省为 latest）唯一确认。 |
| 容器               | Container                             | -      | 通过 Docker 镜像创建的运行实例。                             |
| 容器组             | Pod                                   | -      | Pod 是 Kubernetes 部署应用或服务的最小的基本单位。一个Pod 封装多个应用容器（也可以只有一个容器）、存储资源、一个独立的网络 IP 以及管理控制容器运行方式的策略选项。 |
| 应用               | Application                           | -      | 应用是本平台中的自定义 Kubernetes 资源，由一个或多个关联的计算组件构成整体业务应用，支持通过 UI 编辑模式或 YAML 编排文件创建，运行在开发、测试或生产环境中。 |
| 联邦应用               | Federated Application         | -      | 平台支持在联邦集群的所有成员集群下，跨集群部署和管理相同的应用，实现“两地三中心”容灾备份解决方案。 |
| 计算组件/工作负载  | WorkLoad                              | -      | 平台上构成应用的组件，是基于镜像创建的可以组合提供服务也可以独立运行的程序的统称。 |
| 副本控制器         | Replication Controller                | RC     | RC 确保任何时候 Kubernetes 集群中有指定数量的 pod 副本(replicas)在运行。通过监控运行中的 Pod 来保证集群中运行指定数目的 Pod 副本。指定的数目可以是多个也可以是1个；少于指定数目，RC 就会启动运行新的 Pod 副本；多于指定数目，RC 就会终止多余的 Pod 副本。 |
| 副本集             | Replica Set                           | RS     | ReplicaSet（RS）是 RC 的升级版本，唯一区别是对选择器的支持，RS 能支持更多种类的匹配模式。副本集对象一般不单独使用，而是作为 Deployment 的理想状态参数使用。 |
| 部署               | Deployment                            | -      | Deployment 为 ReplicaSet（副本控制器） 和 Pod（容器组） 提供了声明式的更新。只需要在 Deployment 中描述了对 Replicas（Pods 数量） 和 Containers（容器）的期望状态，Deployment 控制器会将 Pod 和 ReplicaSet 的实际状态改变至期望状态。 |
| 有状态副本集       | StatefulSet                           | -      | 有状态副本集（StatefulSet）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过平台创建一个 StatefulSet 来运行一个程序。也可以在应用中创建多个 StatefulSet 作为应用下的计算组件来为应用提供特定的服务。<br> Kubernetes 在 Deployment 的基础上扩展出的 StatefulSet，在 Pod 被重新创建时，能够恢复 Pod 的状态，满足有状态应用（Stateful Application）的实际使用场景。 |
| 守护进程集         | DaemonSet                             | -      | 守护进程集（DaemonSet）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过平台创建一个 DaemonSet 来运行一个程序。也可以在应用中创建多个 DaemonSet 作为应用下的计算组件来为应用提供特定的服务。<br>DaemonSet 能够保证所有或部分符合条件的主机节点上都能运行 Pod 的副本。 |
| 定时任务           | CronJob                               | -      | 定时任务（CronJob）是 Kubernetes 的一种工作负载控制器（Controllers），您可以通过平台创建一个 CronJob 来定期或重复运行一个非持续性的程序。例如：定时备份、定时清理、定时发送邮件等。<br>平台中的定时任务除支持定时触发方式外，还支持手动触发方式，您可以在创建定时任务时，选择需要的执行方式。 |
| 任务记录           | Job                                   | -      | 定时任务每执行一次，会创建一个任务记录（Job），您可查看任务记录的相关信息，掌握任务的执行情况。 |
| -                  | TApp                                  | -      | TApp 是一种扩展类型的 WorkLoad（工作负载），所属容器组实例具有可以标识的 ID，不同的容器组实例支持使用不同的配置，实现同时运行多版本。 |
| 配置字典           | Configmap                             | -      | Kubernetes 配置字典（Kubernetes ConfigMap）使用键值对保存配置数据，可以保存单个属性，也可以保存配置文件。使用配置，实现对容器化应用的配置管理，使配置与镜像内容分离，保持容器化应用的可移植性。 |
| 保密字典           | Secret                                | -      | Kubernetes Secret 用于在 Kubernetes 集群中储存保密的敏感信息或配置，例如：用户密码、OAuth 令牌、SSH 私钥、访问镜像仓库的认证信息等，建议优先使用保密字典（Kubernetes Secret）储存。 |
| 内部路由           | Service                               | -      | Kubernetes Service 定义了一组 Pod 的逻辑集合，支持设定计算组件在集群内部的访问策略，相当于集群内部的微服务，为其它计算组件或访问者提供位于集群内部的统一访问入口，实现计算组件内部发现的功能。 |
| 访问规则           | Ingress                               | -      | 平台通过使用 Kubernetes Ingress（访问规则），将集群外部的 HTTP/HTTPS 路由暴露给集群内部的 Kubernetes Service，实现计算组件外部访问权控制的功能。 |
| 网络策略           | NetworkPolicy            | -      | 网络策略（NetworkPolicy）是依赖网络插件实现的，从属于命名空间（Namespace） Kubernetes 资源。出于安全、限制网络流量的目的，通过 Pod 选择器（podSelector）和网络流量进（ingress）出（egress） Pod 的规则，实现网络隔离并减少攻击面。 |
| 负载均衡           | Alauda Load Balancer2                 | ALB2   | 负载均衡器（Alauda Load Balancer）作为 Ingress Controller 负责将集群中的流量分发到容器实例。通过负载均衡功能，自动分配计算组件的访问流量，转发给计算组件的容器实例。负载均衡可以提高计算组件的容错能力，扩展计算组件的对外服务能力，提高应用的可用性。 |
| 持久卷声明         | PersistentVolumeClaim                 | PVC    | 持久卷声明是封装了存储资源请求配置的 Kubernetes 资源，是命名空间相关资源。持久卷声明会根据请求参数配置（访问模式，存储大小等），自动匹配集群中合适的持久卷（PersistentVolume，PV）。 |
| 持久卷             | PersistentVolume                      | PV     | 持久卷是表示与集群中后端存储卷映射关系的 Kubernetes 资源，是集群相关资源，负责将实际的存储资源抽象化，成为集群的存储基础设施。 |
| 存储类             | StorageClass                          | -      | 存储类是一种用于描述 Kubernetes 集群可用的存储资源“类别”的 Kubernetes 资源，是集群相关资源。管理员可以根据存储类型、服务质量级别、备份策略或管理员确定的任意策略，通过存储类区分存储资源的类别。 |
| 自定义资源类型     | Custom Resource Definition            | CRD    | Kubernetes 支持用户通过 [CustomResourceDefinition](https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definitions/)  API 自定义 Kubernetes 扩展资源，并能保证新的资源能够快速注册和使用。 |
| 域名               | Domain                                | -      | 管理员通过域名管理功能，可统一管理用于本平台的企业网络域名资源，并通过将域名绑定到项目，实现项目间域名资源的分配和管理。 |
| 子网               | Subnet                                | -      | 支持为使用 Kube-OVN、Calico 容器网络模式的集群划分并管理子网资源，实施网段隔离。 |
| 轻量级目录访问协议 | Lightweight Directory Access Protocol | LDAP   | 平台支持通过同步 LDAP 导入客户企业已有用户体系，用户可通过 LDAP 认证账号登录平台。在平台的在 IDP 配置中可配置 LDAP。 |
| -                  | OpenId Connect                        | OIDC   | 平台支持 OIDC（OpenId Connect）协议，通过在 IDP 配置中添加 OIDC 可以使用本平台认可的第三方平台的账号登录平台 |
| 角色               | Role                                  | -      | 平台通过角色将不同资源的操作权限进行分类和组合，通过为用户分配特定的角色可快速为用户开通或限制对指定资源的操作权限。<br>平台默认设置了五个权限角色：平台管理员、平台审计人员、项目管理员、命名空间管理员、命名空间成员。<br>支持用户根据实际使用场景自定义角色。 |
| 事件               | Event                                 | -      | 平台对接了 Kubernetes 事件，记录了 Kubernetes 资源的重要状态变更及各种运行状态变化的事件。能够在具体资源如集群、应用、任务等出现异常情况时，通过事件分析具体原因。 |
| 审计               | Audit                                 | -      |    平台对接了 Kubernetes 审计，提供了安全相关的时序操作记录，包括：时间、来源、操作结果、发起操作的用户、操作的资源以及请求/响应的详细信息等。通过审计，平台审计员能够清晰的了解 Kubernetes 集群发生的变更。                                                          |
| 日志               | Log                                | -      |    平台对接了 Kubernetes 的日志，能够快速采集 Kubernetes 集群的容器日志，包括容器的标准输出以及容器内的文本文件。同时，支持采集 Kubelet、Docker、Kubernetes 容器化组件的日志。         |
| -                  | Token                                 | -      | 当用户通过 API 访问平台时，可使用平台提供的 Token 进行身份认证。 |
| 业务视图               | -                                     | -      | 在业务视图中，用户可以开发、部署、维护应用；管理配置字典、保密字典、持久卷声明、内部路由、访问规则等命名空间下的资源；对应用实施运维监控。 |
| 管理视图           | -                                     | -      | 管理视图主要面向平台管理员，可管理集群、Kubernetes 资源、网络、存储，并对平台资源进行监控和运维。 |
| 模板仓库           | -                                     | -      | 即 App Catalog，支持平台管理员将远端代码仓库中的应用模板（例如：企业定制开发的 MySQL、Kafka 等中间件应用模板）同步至平台，并通过分配项目配置仓库的权限，控制企业的不同部门或团队访问专属的模板仓库。可供业务人员基于模板快速部署应用，提升部署效率。 |
| 应用市场           | -                                     | -      | 在业务视图的应用市场，开发、测试人员可查看对项目可见的所有应用模版，并可基于应用模版一键部署应用。 |