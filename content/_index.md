+++
title = "Container Platform"
description = ""
+++

# 欢迎使用 Container Platform

Container Platform（以下简称本平台或容器平台）拥抱云原生（Cloud Native）技术，通过整合 Docker 容器、Kubernetes 原生架构、微服务等相关新技术和新理念，可实现业务应用从开发、测试，到部署、运维的全生命周期平台化管理，能有效帮助企业实现数字化转型，提升企业的 IT 交付能力和竞争力。

本平台作为企业级的云原生容器平台，可服务于不同规模的企业，支持管理各种复杂度的基础设施环境（单台机器或多个异构的数据中心）、组织结构清晰的部门建制和人员团队。

本平台能够满足企业级应用逐步向容器化、微服务化过渡的广泛需求，支持企业建立一个覆盖内外部各环节和组织结构的私有云平台。提升企业 IT 资源利用率，加快应用迭代速度，降低应用交付成本，实现业务应用的智能运维，从而助力企业获得持续创新的核心能力。


## 用户手册

<ul class="children children-table">
   <span><span><a href="10usermanual/4adminview"> 管理视图</a></span></span>
   <span><span><a href="10usermanual/3userview"> 业务视图</a></span></span>
   <span><span><a href="10usermanual/platformmanagement"> 平台管理</a></span></span>
   <span><span><a href="10usermanual/projectmanagement"> 项目管理</a></span></span>
   <span><span><a href="10usermanual/setting"> 基础操作</a></span></span>
   <span><span><a href="10usermanual/useraccount"> 登录管理</a></span></span>
</ul>


## API 文档

<ul class="children children-table">
   <span><span><a href="api/01quickstart"> 快速开始</a></span></span>
   <span><span><a href="api/userview"> 业务视图</a></span></span>
   <span><span><a href="api/adminview"> 管理视图</a></span></span>
   <span><span><a href="api/project"> 项目管理</a></span></span>
</ul>


## 常见问题

<ul class="children children-table">
   <span><span><a href="100faq/0"> 平台兼容的 PC 端浏览器有哪些？</a></span></span>
   <span><span><a href="100faq/20"> 如何纳管外部集群及集群下的命名空间？</a></span></span>
   <span><span><a href="100faq/23"> 为容器挂载存储卷时，如何将配置字典中的多个配置项挂载到不同的路径下？</a></span></span>
</ul>




