+++
title = "更新项目和集群的绑定关系信息"
description = "/apis/auth.alauda.io/v1/projectbindings/{name} PUT"
weight = 5
path = "PUT /apis/auth.alauda.io/v1/projectbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projectbindings/{name}" verb="PUT" %}}
