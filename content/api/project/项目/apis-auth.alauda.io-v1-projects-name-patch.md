+++
title = "差异化更新项目"
description = "/apis/auth.alauda.io/v1/projects/{name} PATCH"
weight = 5
path = "PATCH /apis/auth.alauda.io/v1/projects/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projects/{name}" verb="PATCH" %}}
