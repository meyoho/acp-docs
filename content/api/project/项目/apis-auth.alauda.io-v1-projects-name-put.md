+++
title = "更新项目"
description = "/apis/auth.alauda.io/v1/projects/{name} PUT"
weight = 6
path = "PUT /apis/auth.alauda.io/v1/projects/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projects/{name}" verb="PUT" %}}
