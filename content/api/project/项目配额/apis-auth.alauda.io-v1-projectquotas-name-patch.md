+++
title = "差异化更新项目配额"
description = "/apis/auth.alauda.io/v1/projectquotas/{name} PATCH"
weight = 5
path = "PATCH /apis/auth.alauda.io/v1/projectquotas/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projectquotas/{name}" verb="PATCH" %}}
