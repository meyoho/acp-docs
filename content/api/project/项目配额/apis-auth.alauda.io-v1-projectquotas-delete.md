+++
title = "批量删除项目配额"
description = "/apis/auth.alauda.io/v1/projectquotas DELETE"
weight = 10
path = "DELETE /apis/auth.alauda.io/v1/projectquotas"
+++


{{%api path="/apis/auth.alauda.io/v1/projectquotas" verb="DELETE" %}}
