+++
title = "批量解除用户和用户组的绑定关系"
description = "/apis/auth.alauda.io/v1/groupbindings DELETE"
weight = 7
path = "DELETE /apis/auth.alauda.io/v1/groupbindings"
+++


{{%api path="/apis/auth.alauda.io/v1/groupbindings" verb="DELETE" %}}
