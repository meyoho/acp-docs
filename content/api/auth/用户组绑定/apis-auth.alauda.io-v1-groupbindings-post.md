+++
title = "为用户绑定用户组"
description = "/apis/auth.alauda.io/v1/groupbindings POST"
weight = 1
path = "POST /apis/auth.alauda.io/v1/groupbindings"
+++


{{%api path="/apis/auth.alauda.io/v1/groupbindings" verb="POST" %}}
