+++
title = "查看用户和用户组的绑定关系列表"
description = "/apis/auth.alauda.io/v1/groupbindings GET"
weight = 2
path = "GET /apis/auth.alauda.io/v1/groupbindings"
+++

{{%api path="/apis/auth.alauda.io/v1/groupbindings" verb="GET" %}}
