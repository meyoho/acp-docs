+++
title = "查看用户组列表"
description = "/apis/auth.alauda.io/v1/groups GET"
weight = 2
path = "GET /apis/auth.alauda.io/v1/groups"
+++


{{%api path="/apis/auth.alauda.io/v1/groups" verb="GET" %}}
