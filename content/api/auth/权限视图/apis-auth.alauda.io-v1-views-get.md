+++
title = "查看权限视图列表"
description = "/apis/auth.alauda.io/v1/views GET"
weight = 2
path = "GET /apis/auth.alauda.io/v1/views"
+++


{{%api path="/apis/auth.alauda.io/v1/views" verb="GET" %}}
