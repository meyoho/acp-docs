+++
title = "查看用户和角色的绑定关系列表"
description = "/apis/auth.alauda.io/v1/userbindings GET"
weight = 2
path = "GET /apis/auth.alauda.io/v1/userbindings"
+++


{{%api path="/apis/auth.alauda.io/v1/userbindings" verb="GET" %}}
