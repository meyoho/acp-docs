+++
title = "查看通知列表"
description = "/apis/aiops.alauda.io/v1beta1/notifications GET"
weight = 3
path = "GET /apis/aiops.alauda.io/v1beta1/notifications"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notifications" verb="GET" %}}
