+++
title = "删除指定通知"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name} DELETE"
weight = 7
path = "DELETE /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name}" verb="DELETE" %}}
