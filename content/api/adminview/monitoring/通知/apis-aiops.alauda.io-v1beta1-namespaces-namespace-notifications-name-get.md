+++
title = "查看通知详情"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name} GET"
weight = 4
path = "GET /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notifications/{name}" verb="GET" %}}
