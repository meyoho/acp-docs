+++
title = "查看监控模板列表"
description = "/apis/aiops.alauda.io/v1beta1/alerttemplates GET"
weight = 2
path = "GET /apis/aiops.alauda.io/v1beta1/alerttemplates"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/alerttemplates" verb="GET" %}}
