+++
title = "更新监控模板"
description = "/apis/aiops.alauda.io/v1beta1/alerttemplates/{name} PUT"
weight = 5
path = "PUT /apis/aiops.alauda.io/v1beta1/alerttemplates/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/alerttemplates/{name}" verb="PUT" %}}
