+++
title = "批量删除监控模板"
description = "/apis/aiops.alauda.io/v1beta1/alerttemplates DELETE"
weight = 7
path = "DELETE /apis/aiops.alauda.io/v1beta1/alerttemplates"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/alerttemplates" verb="DELETE" %}}
