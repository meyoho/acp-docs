+++
title = "删除指定通知信息"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name} DELETE"
weight = 10
path = "DELETE /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}" verb="DELETE" %}}
