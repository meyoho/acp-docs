+++
title = "更新通知信息的状态"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}/status PUT"
weight = 9
path = "PUT /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}/status"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}/status" verb="PUT" %}}
