+++
title = "更新通知信息"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name} PUT"
weight = 7
path = "PUT /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}" verb="PUT" %}}
