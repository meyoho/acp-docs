+++
title = "差异化更新通知信息"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name} PATCH"
weight = 6
path = "PATCH /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}" verb="PATCH" %}}
