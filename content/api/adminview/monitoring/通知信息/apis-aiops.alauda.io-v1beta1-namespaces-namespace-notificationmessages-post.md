+++
title = "创建通知信息"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages POST"
weight = 1
path = "POST /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages" verb="POST" %}}
