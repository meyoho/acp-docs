+++
title = "查看通知信息的状态"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}/status GET"
weight = 3
path = "GET /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}/status"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}/status" verb="GET" %}}
