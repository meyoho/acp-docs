+++
title = "差异化更新通知信息的状态"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}/status PATCH"
weight = 8
path = "PATCH /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}/status"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}/status" verb="PATCH" %}}
