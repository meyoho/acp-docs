+++
title = "批量删除通知信息"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages DELETE"
weight = 11
path = "DELETE /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages" verb="DELETE" %}}
