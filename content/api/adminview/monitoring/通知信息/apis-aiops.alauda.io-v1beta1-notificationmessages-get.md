+++
title = "查看通知信息列表"
description = "/apis/aiops.alauda.io/v1beta1/notificationmessages GET"
weight = 2
path = "GET /apis/aiops.alauda.io/v1beta1/notificationmessages"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationmessages" verb="GET" %}}
