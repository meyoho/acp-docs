+++
title = "查看通知信息详情"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name} GET"
weight = 4
path = "GET /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages/{name}" verb="GET" %}}
