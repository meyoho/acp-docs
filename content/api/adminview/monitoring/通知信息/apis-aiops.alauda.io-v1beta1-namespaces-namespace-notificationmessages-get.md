+++
title = "查看指定项目下的通知信息列表"
description = "/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages GET"
weight = 5
path = "GET /apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/namespaces/{namespace}/notificationmessages" verb="GET" %}}
