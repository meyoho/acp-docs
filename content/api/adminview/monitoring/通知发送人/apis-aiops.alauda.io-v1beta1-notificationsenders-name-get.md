+++
title = "查看通知发送人详情"
description = "/apis/aiops.alauda.io/v1beta1/notificationsenders/{name} GET"
weight = 3
path = "GET /apis/aiops.alauda.io/v1beta1/notificationsenders/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationsenders/{name}" verb="GET" %}}
