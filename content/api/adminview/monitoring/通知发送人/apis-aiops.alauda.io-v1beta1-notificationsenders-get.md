+++
title = "查看通知发送人列表"
description = "/apis/aiops.alauda.io/v1beta1/notificationsenders GET"
weight = 2
path = "GET /apis/aiops.alauda.io/v1beta1/notificationsenders"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationsenders" verb="GET" %}}
