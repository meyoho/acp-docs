+++
title = "差异化更新通知发送人的信息"
description = "/apis/aiops.alauda.io/v1beta1/notificationsenders/{name} PATCH"
weight = 4
path = "PATCH /apis/aiops.alauda.io/v1beta1/notificationsenders/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationsenders/{name}" verb="PATCH" %}}
