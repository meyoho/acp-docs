+++
title = "更新通知发送人的信息"
description = "/apis/aiops.alauda.io/v1beta1/notificationsenders/{name} PUT"
weight = 5
path = "PUT /apis/aiops.alauda.io/v1beta1/notificationsenders/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationsenders/{name}" verb="PUT" %}}
