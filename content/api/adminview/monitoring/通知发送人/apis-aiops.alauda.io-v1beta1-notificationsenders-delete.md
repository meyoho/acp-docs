+++
title = "批量删除通知发送人的信息"
description = "/apis/aiops.alauda.io/v1beta1/notificationsenders DELETE"
weight = 7
path = "DELETE /apis/aiops.alauda.io/v1beta1/notificationsenders"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationsenders" verb="DELETE" %}}
