+++
title = "删除指定通知发送人的信息"
description = "/apis/aiops.alauda.io/v1beta1/notificationsenders/{name} DELETE"
weight = 6
path = "DELETE /apis/aiops.alauda.io/v1beta1/notificationsenders/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationsenders/{name}" verb="DELETE" %}}
