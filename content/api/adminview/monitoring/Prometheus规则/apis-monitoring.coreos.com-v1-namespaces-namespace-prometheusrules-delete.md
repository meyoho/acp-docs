+++
title = "批量删除 Prometheus 告警规则"
description = "/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules DELETE"
weight = 8
path = "DELETE /apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules"
+++


{{%api path="/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules" verb="DELETE" %}}
