+++
title = "差异化更新 Prometheus 告警规则"
description = "/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name} PATCH"
weight = 5
path = "PATCH /apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name}"
+++


{{%api path="/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules/{name}" verb="PATCH" %}}
