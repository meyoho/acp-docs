+++
title = "查看 Prometheus 告警规则列表"
description = "/apis/monitoring.coreos.com/v1/prometheusrules GET"
weight = 2
path = "GET /apis/monitoring.coreos.com/v1/prometheusrules"
+++


{{%api path="/apis/monitoring.coreos.com/v1/prometheusrules" verb="GET" %}}
