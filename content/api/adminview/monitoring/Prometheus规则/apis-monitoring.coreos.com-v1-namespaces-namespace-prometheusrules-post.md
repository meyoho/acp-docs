+++
title = "创建 Prometheus 告警规则"
description = "/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules POST"
weight = 1
path = "POST /apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules"
+++


{{%api path="/apis/monitoring.coreos.com/v1/namespaces/{namespace}/prometheusrules" verb="POST" %}}
