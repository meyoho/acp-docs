+++
title = "查看通知模板详情"
description = "/apis/aiops.alauda.io/v1beta1/notificationtemplates/{name} GET"
weight = 3
path = "GET /apis/aiops.alauda.io/v1beta1/notificationtemplates/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationtemplates/{name}" verb="GET" %}}
