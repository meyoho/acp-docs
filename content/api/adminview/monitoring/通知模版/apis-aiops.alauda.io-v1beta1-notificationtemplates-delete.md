+++
title = "批量删除通知模板"
description = "/apis/aiops.alauda.io/v1beta1/notificationtemplates DELETE"
weight = 7
path = "DELETE /apis/aiops.alauda.io/v1beta1/notificationtemplates"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationtemplates" verb="DELETE" %}}
