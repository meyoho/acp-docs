+++
title = "差异化更新通知模板"
description = "/apis/aiops.alauda.io/v1beta1/notificationtemplates/{name} PATCH"
weight = 4
path = "PATCH /apis/aiops.alauda.io/v1beta1/notificationtemplates/{name}"
+++


{{%api path="/apis/aiops.alauda.io/v1beta1/notificationtemplates/{name}" verb="PATCH" %}}
