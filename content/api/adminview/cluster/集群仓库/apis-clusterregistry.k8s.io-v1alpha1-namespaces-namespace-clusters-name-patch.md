+++
title = "差异化更新集群"
description = "/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name} PATCH"
weight = 8
path = "PATCH /apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}" verb="PATCH" %}}
