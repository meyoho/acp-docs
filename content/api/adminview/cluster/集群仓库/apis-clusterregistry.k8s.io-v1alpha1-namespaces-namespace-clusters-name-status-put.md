+++
title = "更新集群的状态"
description = "/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}/status PUT"
weight = 7
path = "PUT /apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}/status"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}/status" verb="PUT" %}}
