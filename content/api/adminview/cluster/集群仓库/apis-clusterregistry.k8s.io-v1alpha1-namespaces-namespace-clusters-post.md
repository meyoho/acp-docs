+++
title = "接入集群"
description = "/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters POST"
weight = 1
path = "POST /apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters" verb="POST" %}}
