+++
title = "批量删除集群"
description = "/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters DELETE"
weight = 11
path = "DELETE /apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters" verb="DELETE" %}}
