+++
title = "删除指定集群"
description = "/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name} DELETE"
weight = 10
path = "DELETE /apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}" verb="DELETE" %}}
