+++
title = "差异化更新集群的状态"
description = "/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}/status PATCH"
weight = 6
path = "PATCH /apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}/status"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}/status" verb="PATCH" %}}
