+++
title = "查看集群的状态"
description = "/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}/status GET"
weight = 5
path = "GET /apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}/status"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}/status" verb="GET" %}}
