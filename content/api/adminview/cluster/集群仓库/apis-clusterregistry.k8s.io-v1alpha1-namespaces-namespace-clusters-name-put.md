+++
title = "更新集群"
description = "/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name} PUT"
weight = 9
path = "PUT /apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}" verb="PUT" %}}
