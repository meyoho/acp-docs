+++
title = "查看集群详情"
description = "/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name} GET"
weight = 4
path = "GET /apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}"
+++


{{%api path="/apis/clusterregistry.k8s.io/v1alpha1/namespaces/{namespace}/clusters/{name}" verb="GET" %}}
