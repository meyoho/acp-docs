+++
title = "删除指定的保密字典"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name} DELETE"
weight = 60
path = "DELETE /kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name}" verb="DELETE" %}}
