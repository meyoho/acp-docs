+++
title = "创建保密字典"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets POST"
weight = 20
path = "POST /kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets" verb="POST" %}}
