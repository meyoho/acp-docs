+++
title = "批量删除保密字典"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets DELETE"
weight = 70
path = "DELETE /kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets" verb="DELETE" %}}
