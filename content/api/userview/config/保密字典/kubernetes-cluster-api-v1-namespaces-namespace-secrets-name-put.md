+++
title = "更新保密字典"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name} PUT"
weight = 40
path = "PUT /kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name}" verb="PUT" %}}
