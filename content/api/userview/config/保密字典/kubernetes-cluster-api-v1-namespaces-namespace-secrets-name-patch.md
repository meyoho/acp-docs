+++
title = "差异化更新保密字典"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name} PATCH"
weight = 50
path = "PATCH /kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name}" verb="PATCH" %}}
