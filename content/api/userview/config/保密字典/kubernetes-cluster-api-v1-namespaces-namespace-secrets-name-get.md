+++
title = "查看保密字典详情"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name} GET"
weight = 30
path = "GET /kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets/{name}" verb="GET" %}}
