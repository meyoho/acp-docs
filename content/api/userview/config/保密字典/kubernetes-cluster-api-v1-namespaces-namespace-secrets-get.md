+++
title = "查看保密字典列表"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets GET"
weight = 1
path = "GET /kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/secrets" verb="GET" %}}
