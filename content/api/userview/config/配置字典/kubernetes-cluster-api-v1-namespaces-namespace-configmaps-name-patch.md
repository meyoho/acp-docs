+++
title = "差异化更新配置字典"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name} PATCH"
weight = 50
path = "PATCH /kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name}" verb="PATCH" %}}
