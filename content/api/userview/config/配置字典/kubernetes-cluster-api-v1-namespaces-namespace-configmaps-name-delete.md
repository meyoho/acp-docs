+++
title = "删除指定的配置字典"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name} DELETE"
weight = 60
path = "DELETE /kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name}" verb="DELETE" %}}
