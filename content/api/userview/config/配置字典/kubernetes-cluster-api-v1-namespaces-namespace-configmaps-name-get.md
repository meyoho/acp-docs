+++
title = "查看配置字典详情"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name} GET"
weight = 30
path = "GET /kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name}" verb="GET" %}}
