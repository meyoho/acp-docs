+++
title = "批量删除配置字典"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps DELETE"
weight = 70
path = "DELETE /kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps" verb="DELETE" %}}
