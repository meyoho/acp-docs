+++
title = "创建配置字典"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps POST"
weight = 20
path = "POST /kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps" verb="POST" %}}
