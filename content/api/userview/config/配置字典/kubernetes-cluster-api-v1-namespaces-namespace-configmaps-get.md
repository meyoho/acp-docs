+++
title = "查看配置字典列表"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps GET"
weight = 1
path = "GET /kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps" verb="GET" %}}
