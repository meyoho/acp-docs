+++
title = "更新配置字典"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name} PUT"
weight = 40
path = "PUT /kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/configmaps/{name}" verb="PUT" %}}
