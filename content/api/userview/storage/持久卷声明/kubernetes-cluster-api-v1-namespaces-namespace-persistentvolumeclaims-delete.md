+++
title = "批量删除持久卷声明"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims DELETE"
weight = 100
path = "DELETE /kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims" verb="DELETE" %}}
