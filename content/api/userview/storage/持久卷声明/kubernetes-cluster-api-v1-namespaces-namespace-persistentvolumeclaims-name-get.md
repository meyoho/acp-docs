+++
title = "查看持久卷声明详情"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name} GET"
weight = 30
path = "GET /kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name}" verb="GET" %}}
