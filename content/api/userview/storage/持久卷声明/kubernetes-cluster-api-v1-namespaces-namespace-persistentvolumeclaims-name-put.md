+++
title = "更新持久卷声明"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name} PUT"
weight = 40
path = "PUT /kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name}" verb="PUT" %}}
