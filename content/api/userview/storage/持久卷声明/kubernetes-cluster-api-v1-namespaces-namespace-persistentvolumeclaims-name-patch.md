+++
title = "差异化更新持久卷声明"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name} PATCH"
weight = 50
path = "PATCH /kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name}" verb="PATCH" %}}
