+++
title = "创建持久卷声明"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims POST"
weight = 20
path = "POST /kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims" verb="POST" %}}
