+++
title = "删除指定的持久卷声明"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name} DELETE"
weight = 60
path = "DELETE /kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims/{name}" verb="DELETE" %}}
