+++
title = "查看持久卷声明列表"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims GET"
weight = 1
path = "GET /kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/persistentvolumeclaims" verb="GET" %}}
