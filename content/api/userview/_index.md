+++
title = "业务视图"
description = ""
weight = 3
+++

在业务视图中，用户可以开发、部署、维护应用；管理配置字典、保密字典、持久卷声明、内部路由、访问规则等命名空间下的资源；对应用实施运维监控。

{{% children style="table" depth="1" %}}







