+++
title = "创建有状态副本集"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets POST"
weight = 20
path = "POST /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets" verb="POST" %}}
