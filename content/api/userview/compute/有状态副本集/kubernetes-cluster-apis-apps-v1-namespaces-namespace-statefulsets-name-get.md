+++
title = "查看有状态副本集详情"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name} GET"
weight = 30
path = "GET /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name}" verb="GET" %}}
