+++
title = "差异化更新有状态副本集"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name} PATCH"
weight = 50
path = "PATCH /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name}" verb="PATCH" %}}
