+++
title = "批量删除有状态副本集"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets DELETE"
weight = 100
path = "DELETE /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets" verb="DELETE" %}}
