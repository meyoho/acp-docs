+++
title = "删除指定的有状态副本集"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name} DELETE"
weight = 90
path = "DELETE /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name}" verb="DELETE" %}}
