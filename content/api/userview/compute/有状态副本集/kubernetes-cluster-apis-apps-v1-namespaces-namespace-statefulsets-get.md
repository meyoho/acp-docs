+++
title = "查看有状态副本集列表"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets GET"
weight = 1
path = "GET /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets" verb="GET" %}}
