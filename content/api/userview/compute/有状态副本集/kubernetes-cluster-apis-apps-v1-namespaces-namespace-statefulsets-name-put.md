+++
title = "更新有状态副本集"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name} PUT"
weight =40
path = "PUT /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/statefulsets/{name}" verb="PUT" %}}
