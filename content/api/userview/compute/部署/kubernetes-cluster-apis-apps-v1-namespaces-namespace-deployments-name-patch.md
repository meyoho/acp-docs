+++
title = "差异化更新部署"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name} PATCH"
weight = 70
path = "PATCH /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name}" verb="PATCH" %}}
