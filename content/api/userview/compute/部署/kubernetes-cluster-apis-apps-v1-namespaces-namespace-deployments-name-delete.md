+++
title = "删除指定的部署"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name} DELETE"
weight = 110
path = "DELETE /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name}" verb="DELETE" %}}
