+++
title = "批量删除部署"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments DELETE"
weight = 120
path = "DELETE /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments" verb="DELETE" %}}
