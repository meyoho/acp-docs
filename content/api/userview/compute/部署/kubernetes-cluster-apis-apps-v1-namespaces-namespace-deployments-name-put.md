+++
title = "更新部署"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name} PUT"
weight = 50
path = "PUT /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name}" verb="PUT" %}}
