+++
title = "创建部署"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments POST"
weight = 20
path = "POST /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments" verb="POST" %}}
