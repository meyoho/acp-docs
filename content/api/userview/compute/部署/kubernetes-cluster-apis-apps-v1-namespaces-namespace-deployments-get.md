+++
title = "查看部署列表"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments GET"
weight = 1
path = "GET /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments" verb="GET" %}}
