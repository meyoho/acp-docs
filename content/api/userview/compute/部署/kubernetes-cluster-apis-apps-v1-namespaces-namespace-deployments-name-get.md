+++
title = "查看部署详情"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name} GET"
weight = 30
path = "GET /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/deployments/{name}" verb="GET" %}}
