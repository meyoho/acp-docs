+++
title = "差异化更新定时任务"
description = "/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name} PATCH"
weight = 50
path = "PATCH /kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name}" verb="PATCH" %}}
