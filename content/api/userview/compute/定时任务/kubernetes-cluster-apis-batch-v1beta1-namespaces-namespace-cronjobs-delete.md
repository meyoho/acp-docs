+++
title = "批量删除定时任务"
description = "/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs DELETE"
weight = 100
path = "DELETE /kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs"
+++


{{%api path="/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs" verb="DELETE" %}}
