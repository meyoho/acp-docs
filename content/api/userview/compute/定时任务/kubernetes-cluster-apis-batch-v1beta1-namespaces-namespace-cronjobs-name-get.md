+++
title = "查看定时任务详情"
description = "/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name} GET"
weight = 30
path = "GET /kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name}" verb="GET" %}}
