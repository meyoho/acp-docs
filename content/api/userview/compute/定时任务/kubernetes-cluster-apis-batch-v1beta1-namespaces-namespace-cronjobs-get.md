+++
title = "查看定时任务列表"
description = "/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs GET"
weight = 1
path = "GET /kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs"
+++


{{%api path="/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs" verb="GET" %}}
