+++
title = "删除指定的定时任务"
description = "/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name} DELETE"
weight = 90
path = "DELETE /kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name}" verb="DELETE" %}}
