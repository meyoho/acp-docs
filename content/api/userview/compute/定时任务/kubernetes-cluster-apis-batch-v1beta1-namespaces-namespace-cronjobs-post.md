+++
title = "创建定时任务"
description = "/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs POST"
weight = 20
path = "POST /kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs"
+++


{{%api path="/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs" verb="POST" %}}
