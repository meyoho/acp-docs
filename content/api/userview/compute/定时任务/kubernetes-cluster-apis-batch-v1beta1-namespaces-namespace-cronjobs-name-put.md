+++
title = "更新定时任务"
description = "/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name} PUT"
weight = 40
path = "PUT /kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/batch/v1beta1/namespaces/{namespace}/cronjobs/{name}" verb="PUT" %}}
