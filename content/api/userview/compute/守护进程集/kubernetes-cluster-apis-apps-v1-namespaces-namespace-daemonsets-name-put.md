+++
title = "更新守护进程集"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name} PUT"
weight = 40
path = "PUT /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name}" verb="PUT" %}}
