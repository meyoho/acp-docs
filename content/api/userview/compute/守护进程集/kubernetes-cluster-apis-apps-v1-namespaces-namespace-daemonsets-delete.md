+++
title = "批量删除守护进程集"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets DELETE"
weight = 100
path = "DELETE /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets" verb="DELETE" %}}
