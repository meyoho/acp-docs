+++
title = "查看守护进程集详情"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name} GET"
weight = 30
path = "GET /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name}" verb="GET" %}}
