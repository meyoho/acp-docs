+++
title = "创建守护进程集"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets POST"
weight = 20
path = "POST /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets" verb="POST" %}}
