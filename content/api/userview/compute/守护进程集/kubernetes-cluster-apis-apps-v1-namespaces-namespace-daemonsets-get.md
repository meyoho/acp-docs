+++
title = "查看守护进程集列表"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets GET"
weight = 1
path = "GET /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets" verb="GET" %}}
