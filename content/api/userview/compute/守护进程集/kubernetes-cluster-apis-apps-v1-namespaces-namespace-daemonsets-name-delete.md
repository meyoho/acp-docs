+++
title = "删除指定的守护进程集"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name} DELETE"
weight = 90
path = "DELETE /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name}" verb="DELETE" %}}
