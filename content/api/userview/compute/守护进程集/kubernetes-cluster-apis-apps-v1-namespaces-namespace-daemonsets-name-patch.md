+++
title = "差异化更新守护进程集"
description = "/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name} PATCH"
weight = 50
path = "PATCH /kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/apps/v1/namespaces/{namespace}/daemonsets/{name}" verb="PATCH" %}}
