+++
title = "删除指定的内部路由"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name} DELETE"
weight = 100
path = "DELETE /kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name}" verb="DELETE" %}}
