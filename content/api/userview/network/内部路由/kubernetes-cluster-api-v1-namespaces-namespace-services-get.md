+++
title = "查看内部路由列表"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services GET"
weight = 1
path = "GET /kubernetes/{cluster}/api/v1/namespaces/{namespace}/services"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services" verb="GET" %}}
