+++
title = "创建内部路由"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services POST"
weight = 20
path = "POST /kubernetes/{cluster}/api/v1/namespaces/{namespace}/services"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services" verb="POST" %}}
