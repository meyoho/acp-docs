+++
title = "更新内部路由"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name} PUT"
weight = 40
path = "PUT /kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name}" verb="PUT" %}}
