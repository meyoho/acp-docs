+++
title = "差异化更新内部路由"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name} PATCH"
weight = 50
path = "PATCH /kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name}" verb="PATCH" %}}
