+++
title = "查看内部路由详情"
description = "/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name} GET"
weight = 30
path = "GET /kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name}"
+++


{{%api path="/kubernetes/{cluster}/api/v1/namespaces/{namespace}/services/{name}" verb="GET" %}}
