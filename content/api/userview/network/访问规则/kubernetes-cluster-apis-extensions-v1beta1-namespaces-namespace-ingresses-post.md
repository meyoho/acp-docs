+++
title = "创建访问规则"
description = "/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses POST"
weight = 20
path = "POST /kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses"
+++


{{%api path="/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses" verb="POST" %}}
