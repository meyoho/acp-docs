+++
title = "查看访问规则列表"
description = "/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses GET"
weight = 1
path = "GET /kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses"
+++


{{%api path="/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses" verb="GET" %}}
