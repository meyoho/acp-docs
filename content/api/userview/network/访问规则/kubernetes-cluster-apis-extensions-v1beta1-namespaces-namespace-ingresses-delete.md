+++
title = "批量删除访问规则"
description = "/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses DELETE"
weight = 100
path = "DELETE /kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses"
+++


{{%api path="/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses" verb="DELETE" %}}
