+++
title = "删除指定的访问规则"
description = "/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name} DELETE"
weight = 60
path = "DELETE /kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name}" verb="DELETE" %}}
