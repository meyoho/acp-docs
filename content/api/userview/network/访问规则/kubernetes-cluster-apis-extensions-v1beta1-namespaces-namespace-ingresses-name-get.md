+++
title = "查看访问规则详情"
description = "/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name} GET"
weight = 30
path = "GET /kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name}" verb="GET" %}}
