+++
title = "更新访问规则"
description = "/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name} PUT"
weight = 40
path = "PUT /kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name}" verb="PUT" %}}
