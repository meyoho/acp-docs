+++
title = "差异化更新访问规则"
description = "/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name} PATCH"
weight = 50
path = "PATCH /kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name}"
+++


{{%api path="/kubernetes/{cluster}/apis/extensions/v1beta1/namespaces/{namespace}/ingresses/{name}" verb="PATCH" %}}
