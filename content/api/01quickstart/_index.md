+++
title = "快速开始"
description = "为您介绍 API 的调用方法和认证方式，帮助您快速了解如何使用我们提供的 API 开发您的应用。"
weight = 1
+++

为您介绍 API 的调用方法和认证方式，帮助您快速了解如何使用我们提供的 API 开发您的应用。

**文档约定**

在您阅读本文档之前，请先了解文档中的约定内容，以便您更好的使用我们提供的服务。

<style>table th:first-of-type { width: 20%;}</style> 

|  约定内容  |  说明   |
| ------- |   --------  |
|   `{}` 内参数为变量  |   在文档中该符号内参数为变量参数，在调用接口过程中，需要替换为实际环境的参数。例如：查看用户详情接口请求行 `GET /apis/auth.alauda.io/v1/users/{name}`，其中的 `{name}` 需要在调用接口时替换为实际的用户名称。      |



- [API 调用方法]({{< relref "api/01quickstart/01url.md" >}})

- [认证鉴权]({{< relref "api/01quickstart/02authentication.md" >}})

- [返回结果]({{< relref "api/01quickstart/03response.md" >}})

