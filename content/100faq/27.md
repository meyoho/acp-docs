+++
title = "如何通过平台管理 HTTPS 证书？"
description = ""
weight = 100
+++

业务视图创建访问规则时，如果需要通过 HTTPS 协议访问，需要上传 HTTPS 证书。而 HTTPS  证书需要在创建访问规则之前，以保密字典（TLS 类型）的形式在访问规则创建的命名空间下创建好。

**操作步骤**

1. 参照 [创建保密字典]({{< relref "10usermanual/3userview/20config/2secret/1secretcreate.md" >}}) 在待创建访问规则命名空间中，创建 TLS 类型的保密字典，将 HTTPS 证书的内容保存在保密字典中。

2. 创建访问规则时，在 HTTPS 下拉选择框中，选择已创建的保密字典。

	<img src="/img/faqingress.png" width = "600" />
​	
​	







