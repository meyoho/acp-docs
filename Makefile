PWD=$(shell pwd)
TAG=dev-$(shell git config --get user.email | sed -e "s/@/-/")
IMAGE=index.alauda.cn/alaudaorg/acp-docs
image=$(IMAGE)
tag=$(TAG)

.PHONE: setup


image:
	docker build -t acp-docs -f Dockerfile .

local: image
	docker run --rm -it -p 80:80 acp-docs

setup:
	chmod +x ./install.sh
	./install.sh

run:
	kubectl devops apidocs run

apidocs:
	kubectl devops apidocs merge --config apidocs.yaml --force
	kubectl devops apidocs translate --config apidocs.yaml --output-file translations/en.json
	cp translations/en.json translations/zh.json
	kubectl devops apidocs convert --config apidocs.yaml --input-folder=translations --output-folder=i18n
	cp i18n/en.json zh.json
	kubectl devops apidocs generate --config apidocs.yaml --data-folder data

clean:
	rm -fr content/api
	rm -f data/*
	rm -f translations/*
	rm -f i18n/*


